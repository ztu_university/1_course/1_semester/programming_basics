#include <stdio.h>
#include <math.h>

int getNumLen(int number);
void splitDigits(int number, int digit[], int numLen);
int checkDigits(int digit[], int numLen, int number);
int checkCondition(int number, int digit[]);

int main(void) {
    printf ("Set the range between [-9999; -1000] or [1000; 9999]:\t");
    int idx_i, upb_i;
    scanf ("%d%d", &idx_i, &upb_i);
    char sign = '+';
    if ((upb_i >= -9999 && upb_i <= -1000)  && 
        (idx_i >= -9999 && idx_i <= -1000)  && 
        (idx_i - upb_i <= 0)) {
        int temp = idx_i;
        idx_i = abs(upb_i);
        upb_i = abs(temp);
        sign = '-';
    }
    if ((idx_i >= 1000 && idx_i <= 9999)   && 
        (upb_i >= 1000 && upb_i <= 9999)  && 
        (upb_i - idx_i >= 0)) {
        int counter = 1;
        while (idx_i <= upb_i) {
            int numLen = getNumLen(idx_i);
            int digit[numLen];
            splitDigits(idx_i, digit, numLen);
            int bool_ = checkDigits(digit, numLen, idx_i);
            if (bool_ == 1) {
                printf ("%d. %c%d\n", counter++, sign, idx_i);
            }
            idx_i++;
        }
    }
    return 0;
}

int getNumLen(int idx_i) {
    int len = 0;
    do {
        idx_i /= 10;
        len++;
    } while (idx_i > 0);
    return len;
}

void splitDigits(int idx_i, int digit[], int numLen) {
    int d_i = 0;
    while (idx_i > 0) {
        digit[d_i] = idx_i % 10;    // setting from lower to upper digit
        idx_i /= 10;
        d_i++;
    }
}

int checkDigits(int digit[], int numLen, int idx_i) {

    for (int i = 0; i < numLen; i++) {
        for (int j = i + 1; j < numLen; j++) {
            if (digit[i] == digit[j]) {
                return 0;
            }
        }
    }
    return checkCondition(idx_i, digit);
}

int checkCondition(int number, int digit[]) {
    int ab = number / 100;
    int cd = number % 100;
    if ((ab - cd) == digit[3] + digit[2] + digit[1] + digit[0]) {
        return 1;
    }
    return 0;
}