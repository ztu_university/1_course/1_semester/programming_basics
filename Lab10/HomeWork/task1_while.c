#include <stdio.h>
#include <math.h>

int main(void) {
    int idx_i = 1, idx_j = 1;
    int upb_i = 10, upb_j = 10;
    double sum = 0.0;

    for (idx_i; idx_i <= upb_i; idx_i++) {
        while (idx_j <= upb_j) {
            sum += cos(idx_i + 1.0) / idx_j;
            idx_j++;
        }
        idx_j = 1;
    }
    printf ("%.10lf", sum);
    return 0;
}