#include <stdio.h>

int main(void) {
    char ch = 48;
    int sqLen = 8;
    int step = 0;
    for (int j = 0; j < sqLen; j++) {
        for (int i = 0; i < sqLen; i++) {
            if (step == i) {
                printf ("%c ", ch);    
            } else {
                printf ("  ");        
            }
        }
        printf ("\n");
        step++;
    }
    step = 7;
    for (int j = 0; j < sqLen; j++) {
        for (int i = 0; i < sqLen; i++) {
            if (step == i) {
                printf ("%c ", ch);    
            } else {
                printf ("  ");        
            }
        }
        printf ("\n");
        step--;
    }
    return 0;
}