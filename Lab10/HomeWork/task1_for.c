/**
 * Unneccesary file
**/

#include <stdio.h>
#include <math.h>

int main(void) {
    int upb_i = 10, upb_j = 10;
    double sum = 0.0;

    for (int idx_i = 1; idx_i <= upb_i; idx_i++) {
        for (int idx_j = 1; idx_j <= upb_j; idx_j++) {
            sum += cos(idx_i + 1.0) / idx_j;
        }
    }
    printf ("%.10lf", sum);
    return 0;
}