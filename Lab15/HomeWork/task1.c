#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void setArr(int* arrp, int size);
int selectRow(int size);
void sortRow(int* arrp, int size, int row);
void printArray(int* arrp, int size);

int main() {
    printf ("Set size of square:\t");
    int n;
    scanf ("%d", &n);
    if (n <= 0) {
        printf ("Error size\nProgramm terminated\n");
        return 1;
    }
    int arr[n][n];
    int* arrp = (int*)(arr);
    setArr(arrp, n);
    printArray(arrp, n);

    int inSelectRowMode = 1;
    while (inSelectRowMode) {
        int r = selectRow(n);
        if (r == -1) {
            printf ("Error row\n");
            inSelectRowMode = 0;
        } else {
            sortRow (arrp, n, r);
            printf ("%d row sorted\n", r + 1);
            printArray (arrp, n);
        }
    }
    return 0;
}

void setArr(int* arr, int n) {
    srand(time(0));
    rand();
    for (int a = 0; a < n; a++) {
        for (int b = 0; b < n; b++) {
            (arr[a*n + b]) = rand() % 20 - 10;
        }
    }
}

int selectRow(int n) {
    int r;
    printf ("Select the row to sort in descending order:\t");
    scanf ("%d", &r);
    r = r - 1;
    if (r < 0 || r >= n) {
        return -1;
    }
    return r;   
}

void sortRow(int* arr, int n, int r) {
    int sorted = 0;
    while (!sorted) {
        sorted = 1;
        for (int i = 1; i < n-1; i+=2) {
            if (arr[r*n+i] < arr[r*n+i+1]) {
                int temp = arr[r*n+i];
                arr[r*n+i] = arr[r*n+i+1];
                arr[r*n+i+1] = temp;
                sorted = 0;
            }
        }

        for (int i = 0; i < n-1; i+=2) {
            if (arr[r*n+i] < arr[r*n+i+1]) {
                int temp = arr[r*n+i];
                arr[r*n+i] = arr[r*n+i+1];
                arr[r*n+i+1] = temp;
                sorted = 0;
            }
        }
    }
}

void printArray(int* arr, int n) {
  for (int a = 0; a < n; a++) {
        for (int b = 0; b < n; b++) {
            printf ("%3d ", (arr[a*n + b]));
        }
        printf ("\n\n");
    }
}