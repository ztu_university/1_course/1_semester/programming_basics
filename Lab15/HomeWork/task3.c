#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void setArr(float* arrp, int size);
void selectionSort(float* arr, int size);
void insertionSort(float* arr, int size);
void printArr(float* arr, int heigt, int length);

int main() {
    printf ("Set height and length of rectangle:\t");
    int h, l;
    scanf ("%d %d", &h, &l);
    if (h <= 0 || l <= 0) {
        printf ("Error size\nProgramm terminated\n");
        return 1;
    }
    float initArr[h][l];
    float* arrp = (float*)(initArr);
    setArr (arrp, h*l);
    printf ("\nBefore sort\n");
    printArr (arrp, h, l);
    printf ("Choose sorting algorithm\n1 - selctionSort"
            "\t2 - insertionSort\n");
    int option;
    scanf ("%d", &option);
    if (option == 1) {
        selectionSort (arrp, h*l);
        printf ("\nAfter selection sort\n");
        printArr (arrp, h, l);
    } else if (option == 2) {
        insertionSort (arrp, h*l);
        printf ("\nAfter insertion sort\n");
        printArr (arrp, h, l);
    } else {
        printf ("Wrong option\n");
    }
    return 0;
}

void setArr(float* arr, int n) {
    srand(time(0));
    rand();
    for (int i = 0; i < n; i++) {
        arr[i] = (rand() / (float)RAND_MAX) * 10; // 0 10
    }
}

void selectionSort(float* arr, int n) {
    int i, j, min_idx;
    float temp;
    for (i = 0; i < n-1; i++) {
        min_idx = i;
        for (j = i+1; j < n; j++)
            if (arr[j] < arr[min_idx]) {
                min_idx = j;
            }
        temp = arr[i];
        arr[i] = arr[min_idx];
        arr[min_idx] = temp;
    }
}

void insertionSort(float* arr, int n) {
    int i, j;
    float key;
    for (i = 1; i < n; i++) {
        key = arr[i];
        j = i - 1;

        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}

void printArr(float* arr, int hei, int len) {
    for (int i = 0; i < hei; i++) {
        for (int j = 0; j < len; j++) {
            printf ("%7.3f ", arr[i*len + j]);
        }
        printf ("\n\n");
    }
}