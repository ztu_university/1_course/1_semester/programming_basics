#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void setArr(int* arrp, int height, int length);
void printArray(int* arrp, int height, int length);
void delRowColWithMaxElem(int* arrp, int height, int length);

int main() {
    printf ("Set height and length of rectangle:\t");
    int hei, len;
    scanf ("%d %d", &hei, &len);
    if (hei <= 0 || len <= 0) {
        printf ("Error size\nProgramm terminated\n");
        return 1;
    }
    int arr[hei][len];
    int* arrp = (int*)(arr);
    setArr(arrp, hei, len);
    printArray(arrp, hei, len);
    printf ("\n");
    delRowColWithMaxElem(arrp, hei, len);
    //printArray(arrp, hei, len);
    return 0;
}

void setArr(int* arr, int hei, int len) {
    srand(time(0));
    rand();
    for (int a = 0; a < hei; a++) {
        for (int b = 0; b < len; b++) {
            (arr[a*len + b]) = rand() % 20 - 10;
        }
    }
}

void printArray(int* arr, int hei, int len) {
  for (int a = 0; a < hei; a++) {
        for (int b = 0; b < len; b++) {
            printf ("%6d ", (arr[a*len + b]));
        }
        printf ("\n\n");
    }
}


/**
 * 1) find the last max element in array and save it's 
      position (1st loop)

 * 2) check the gap between 0 element and last max element 
      if there is more max elements (2nd loop)
 * 2.1) if so, count exact size of new array
 * 2.2) if no more were found - do step 3
  
 * 3) copy necessary elements from initial array to a new 
      array (3rd loop)
 * 3.1) count height of new array
**/
void delRowColWithMaxElem(int* arr, int hei, int len) {
    // find indexes of last max element
    int max = arr[0];
    int a_idx = 0, b_idx = 0;
    int sum = hei * len;
    int maxElCounter = 0;
    for (int a = 0; a < hei; a++) {
        for (int b = 0; b < len; b++) {
            if (max <= arr[a*len + b]) {
                max = arr[a*len + b];
                a_idx = a;
                b_idx = b;
                maxElCounter++;
            }
        }
    }
    printf ("max element = %d\n", max);
    // counting new size of array
    int totalSize = sum;
    int tempSum = 0;
    int tempHei = hei;
    int tempLen = len;
    int a_switch = 0;
    int b_switch = 0;
    // can be optimized by less incrementing

    // don't do unecessary moves after last max element's height (a <= a_idx)
    for (int a = 0; a <= a_idx; a++) {
        for (int b = 0; b < len; b++) {
            // find max element
            if (max == arr[a*len + b]) {
                // columns
                for (int i = a - 1; i >= 0; i--) {
                    // checking if any element above (top side) is max
                    // if so, decrement height and change tempSum
                    if (arr[a*len + b] == arr[i*len + b]) {
                        tempLen++;
                        tempSum = tempLen - 1;
                        a_switch = 1;
                        tempLen--;
                        tempHei--;
                        break;
                    }
                }
                // rows
                for (int j = b - 1; j >= 0; j--) {
                    // checking if any element behind (left side) is max
                    // if so, decrement length and change tempSum
                    if (arr[a*len + b] == arr[a*len + j]) {
                        tempHei++;
                        tempSum = tempHei - 1;
                        b_switch = 1;
                        tempHei--;
                        tempLen--;
                        break;
                    }
                }
                // if elements were found above and behind - don't change tempSum
                if (a_switch == 1 && b_switch == 1) {
                    tempSum = 0;
                // if no elements were found - decrement height and length
                } else if (a_switch == 0 && b_switch == 0) {
                    tempSum = tempHei + tempLen - 1;
                    tempHei--;
                    tempLen--;
                }
                totalSize -= tempSum;
                tempSum = 0;
                a_switch = 0;
                b_switch = 0;
            }
        }
    }
    printf ("totalSize = %d\n", totalSize);
    int newArr[totalSize];

    int row = 0;
    int col = 0;
    int idx = 0;
    int processRow = 1;

    int newHei = 1;
    // copy necessary elements into new array
    if (totalSize != 0) {
        while (totalSize != idx) {
            int switch_a = 1;
            int switch_b = 0;
            int switch_a_pair = 1;
            int switch_b_pair = 1;
            // checking for max element in a current row
            if (processRow) {
                col = 0;
                for (int a = 0; a < len; a++) {
                    if (arr[row*len + a] != max) {
                        switch_a = 1;
                    } else {
                        switch_a = 0;
                        row++;
                        break;
                    }
                }
            }
            // checking for max element in a current column
            if (switch_a == 1) {
                for (int b = 0; b < hei; b++) {
                    if (arr[b*len + col] != max) {
                        switch_b = 1;
                    } else {
                        switch_b = 0;
                        processRow = 0;
                        col++;
                        if (col == len) {
                            processRow = 1;
                            row++;
                        }
                        break;
                    }
                }
            }
            if (switch_b == 1) {
                newArr[idx] = arr[row*len + col];
                idx++;
                col++;
                if (col == len) {
                    processRow = 1;
                    row++;
                } else {
                    processRow = 0;
                }
                // going downward on a column to calculate height
                if (newHei == 1) {
                    for (int b = row + 1; b < hei; b++) {
                        for (int a = 0; a < len; a++) {
                            if (arr[b*len + a] != max) {
                                switch_b_pair = 1;
                            } else {
                                switch_b_pair = 0;
                                break;
                            }
                        }
                        if (switch_b_pair == 1) {
                            newHei++;
                        }
                    }
                }
            }
        }
        printf ("\n");
        printArray (newArr, newHei, totalSize / newHei);
    }
}