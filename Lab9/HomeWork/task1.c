#include <stdio.h>

int main(void) {
    printf ("Set positive numbers\n");
    int num;
    scanf ("%d", &num);
    int sum = num;
    int numCount = 1;
    int cmd;
    while (num >= 0) {
        printf ("1 - Continue\t\t2 - Stop\n");
        scanf ("%d", &cmd);
        if (cmd == 1) {
            scanf ("%d", &num);
            sum += num;
            numCount++;
        } else if (cmd == 2) {
            printf ("Arithmetic mean = %.2f", (float)sum/numCount);
            break;
        } else  {
            printf ("\nYou set wrong command: %d\n", cmd);
        }
    }    
    return 0;
}