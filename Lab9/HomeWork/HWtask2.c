#include <stdio.h>
#include <windows.h>

int main(void) {
    int i = 1;
    unsigned int time;
    while (i) {
        printf ("Set sleep time\n");
        scanf ("%d", &time);
        time *= 1000;
        Sleep (time);
        Beep (14000, time);
        printf ("1 - Continue\t2 - Stop\n");
        scanf ("%d", &i);
        if (i == 1) {
            ;
        } else if (i == 2) {
            i = 0;
        } else {
            printf ("Wrong command\n");
            i = 0;
        }
    }
    return 0;
}