#include <stdio.h>
#include <math.h>

int main(void) {
    printf ("Set a 4-digit number\n");
    int num, digit, sum = 0, prod = 1, step = 1;
    scanf ("%d", &num);
    int cmd = 1;
    while (cmd == 1) {
        
        if ((num >= 1000 && num <= 9999) ||
            (num <= -1000 && num >= -9999)) {
            while (num != 0) {
                digit = abs(num % 10);
                num /= 10;
                if (step < 3) {
                    prod *= digit;
                } else {
                    sum += digit;
                }
                step++;
            }
            printf ("Product of last 2 digits:\t%d\nSum of first 2 digits:\t\t%d\n", prod, sum);
        } else {
            printf ("\nYou set wrong number: %d\nTry again\n", num);
        }
        printf ("1 - Continue\t\t2 - Stop\n");
        scanf ("%d", &cmd);
        if (cmd == 1) {
            scanf ("%d", &num);
            sum = 0;
            prod = 1;
            step = 1;
        } else if (cmd == 2) {
            break;
        } else  {
            printf ("\nYou set wrong command: %d\n", cmd);
        }
    }
    return 0;
}