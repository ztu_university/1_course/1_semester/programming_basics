#include <stdio.h>
#include <windows.h>

int main (void) 
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);
  double a, b, c;
  scanf ("%lf %lf %lf", &a, &b, &c);
  double y = ((0.5*a + 0.75*b - 1.4) / (3*c + 1)) + (1 / (a - c));
  printf ("\ny = %.15lf\n", y);
  return 0;
}
