#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void allocMem(long long int bytes, float** arr, int height);
void setArr(float** arr, int height, int length);
void printArray(float** arr, int height, int length);
int findMaxElIdx(float** arr, int height, int length);
int findMinElIdx(float** arr, int height, int length);
void delRows(float** arr, int* height, int length, int maxIdx, int minIdx);

int main () {
    printf ("Set height and length of array:\n");
    int hei, len;
    scanf ("%d %d", &hei, &len);
    if (hei <= 0 || len <= 0) {
        printf("Invalid size\nProgramm terminated\n");
        return 1;
    }
    int* newHei = &hei;
    float* arr[*newHei];
    long long int bytes = len * sizeof(float);
    
    allocMem (bytes, arr, hei);
    setArr (arr, hei, len);
    printArray (arr, hei, len);
    printf("+-+-+-+-+-+-+-+-+-+\n\n");
    int maxIdx = findMaxElIdx(arr, hei, len);
    int minIdx = findMinElIdx(arr, hei, len);
    delRows(arr, &hei, len, maxIdx, minIdx);
    printArray (arr, hei, len);
    return 0;
}

void allocMem(long long int bytes, float** arr, int hei) {
    for (int i = 0; i < hei; i++) {
        arr[i] = (float*)malloc(bytes);
    }
}

void setArr(float** arr, int hei, int len) {
    srand(time(0));
    rand();
    for (int a = 0; a < hei; a++) {
        for (int b = 0; b < len; b++) {
            arr[a][b] = ((rand() % 2000) - 1000) / 100.0f;
        }
    }
}

void printArray(float** arr, int hei, int len) {
    for (int a = 0; a < hei; a++) {
        for (int b = 0; b < len; b++) {
            printf ("%5.2f ", arr[a][b]);
        }
        printf ("\n\n");
    }
}

int findMaxElIdx(float** arr, int hei, int len) {
    float* max = &arr[0][0];
    int maxIdx = 0; // row idx
    for (int a = 0; a < hei; a++) {
        for (int b = 0; b < len; b++) {
            if (*max < arr[a][b]) {
                max = &arr[a][b];
                maxIdx = a;
            }
        }
    }
    return maxIdx;
}

int findMinElIdx(float** arr, int hei, int len) {
    float* min = &arr[0][0];
    int minIdx = 0; // row idx
    for (int a = 0; a < hei; a++) {
        for (int b = 0; b < len; b++) {
            if (*min > arr[a][b]) {
                min = &arr[a][b];
                minIdx = a;
            }
        }
    }
    return minIdx;
}

void delRows(float** arr, int* hei_, int len, int maxIdx, int minIdx) {
    // check if both indexes are similar
    int hei = *hei_;
    if (maxIdx == minIdx) {
        for (int i = maxIdx; i < hei - 1; i++) {
            for (int j = 0; j < len; j++) {
                arr[i][j] = arr[i + 1][j];
            }
        }
        free(arr[hei - 1]);
        *hei_ = *hei_ - 1;
    } else {
        if (maxIdx < minIdx) {
            int tempIdx = maxIdx;
            maxIdx = minIdx;
            minIdx = tempIdx;
        }
        for (int i = minIdx; i < hei - 1; i++) {
            for (int j = 0; j < len; j++) {
                arr[i][j] = arr[i + 1][j];
            }
        }
        maxIdx--;
        for (int i = maxIdx; i < hei - 2; i++) {
            for (int j = 0; j < len; j++) {
                arr[i][j] = arr[i + 1][j];
            }
        }
        free(arr[hei - 1]);
        free(arr[hei - 2]);
        *hei_ = *hei_ - 2;
    }
}