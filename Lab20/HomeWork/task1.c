#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
const int size = 4;

void setArr(float* arrp, int size);
void printArray(void* arrType, int size, int dimension);
void copyMaxElemCol(float** newArr, float* arrp, int size);

int main() {
    float arr[size][size];
    float* newArr[size];
    float* arrp = (float*)(arr);

    printf("First 4 addresses in arrp\n");
    printf("%p\n", &(arrp[0]));
    printf("%p\n", &(arrp[1]));
    printf("%p\n", &(arrp[2]));
    printf("%p\n", &(arrp[3]));

    void* arr2DType = arrp;
    void* arr1DType = newArr;

    setArr (arrp, size);
    printArray (arr2DType, size, 2);
    copyMaxElemCol(newArr, arrp, size);
    printf ("\nAdresses from arrp (checking if they "
            "are the same as copyMaxElemCol function printed)\n");
    printArray (arr1DType, size, 1);
    return 0;
}

void setArr(float* arr, int n) {
    srand(time(0));
    rand();
    for (int a = 0; a < n; a++) {
        for (int b = 0; b < n; b++) {
            (arr[a*n + b]) = ((rand() % 2000) - 1000) / 100.0f;
        }
    }
}

void printArray(void* arrType, int n, int dim) {
    if (dim == 2) {
        float* arr = (float*)arrType;
        for (int a = 0; a < n; a++) {
            for (int b = 0; b < n; b++) {
                printf ("%5.2f ", arr[a*n + b]);
            }
            printf ("\n\n");
        }
    } else if (dim == 1) {
        // print addresses
        float** newArr = (float**)arrType;
        for (int b = 0; b < n; b++) {
            printf ("%f\t%p\n", *newArr[b], &(*newArr[b]));
        }
        printf ("\n\n");
    }
}

void copyMaxElemCol(float** newArr, float* arrp, int size) {
    float max;
    int idx;
    for (int j = 0; j < size; j++) {
        max = arrp[j];
        idx = j;
        for (int i = 0; i < size; i++) {
            if (arrp[j + size*i] > max) {
                max = arrp[j + size*i];
                idx = j + size*i;
            }
        }
        newArr[j] = &arrp[idx];
        printf("%f\t%p\n", *newArr[j], &(*newArr[j]));
    }
}