#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void allocMem(long long int bytes, float* arr);
void setArr(float* arr, int length);
void copyArrs(float* initArr, int initLen, float* arr1, int len1, float* arr2, int len2);
void printArray(float* arr, int length);
int findSizeAndMutualElements_a(float* arr_to_operate, int len, int idx1, float* cachedArr);
int findAllMutualElementsAndCopy(float* arr_to_operate, float* handleArr, int len);
void copyMutualElements_b(float* initArr, float* cachedArr, int initArrLen);

int main () {
    printf ("Set size of array X and Y:\n");
    int a, b;
    scanf ("%d %d", &a, &b);
    if (a < 0 || b < 0) {
        printf("Invalid size\nProgramm terminated\n");
        return 1;
    }

    float x[a], y[b];
    long long int bytes_x = a * sizeof(float);
    long long int bytes_y = b * sizeof(float);

    srand(time(0));
    rand();

    allocMem (bytes_x, x);
    setArr (x, a);
    printArray (x, a);
    printf("+-+-+-+-+-+-+-+-+-+\n\n");

    allocMem (bytes_y, y);
    setArr (y, b);
    printArray (y, b);
    printf("+-+-+-+-+-+-+-+-+-+\n\n");

    int ab = a + b;
    float xy[ab];
    long long int bytes_xy = ab * sizeof(float);
    allocMem (bytes_xy, xy);
    copyArrs (xy, ab, x, a, y, b);

    float cachedArr[ab];
    int parr_size = findSizeAndMutualElements_a(xy, ab, a, cachedArr);
    float parr[parr_size];

    float cachedArr_[parr_size];
    int parr_size_ = findAllMutualElementsAndCopy(cachedArr, cachedArr_, parr_size);
    float newArr[parr_size_];

    copyMutualElements_b(newArr, cachedArr_, parr_size_);
    printf("+-+-+-+-+-+-+-+-+-+\n\n");
    return 0;
}

void allocMem(long long int bytes, float* arr) {
    arr = (float*)malloc(bytes);
}

void setArr(float* arr, int len) {
    for (int b = 0; b < len; b++) {
        arr[b] = ((rand() % 10) - 5);// / 10.0f;
    }
}

void printArray(float* arr, int len) {
    for (int b = 0; b < len; b++) {
        printf ("%3.f ", arr[b]);
    }
    printf ("\n\n");
}

void copyArrs(float* initArr, int initLen, float* arr1, int len1, float* arr2, int len2) {
    int __i = initLen - 1;
    for (int i = len2 - 1; i != -1; i--) {
        initArr[__i] = arr2[i];
        __i--;
    }

    for (int i = len1 - 1; i != -1; i--) {
        initArr[__i] = arr1[i];
        __i--;
    }
}

int findSizeAndMutualElements_a(float* arrto, int len, int idx1, float* cachedArr) {
    int iarr_size = 0;
    cachedArr[0] = arrto[0];
    int counter_l = 1;
    int key = 1;
    for (int i = 1; i < idx1; i++) {
        for (int j = 0; j < idx1; j++) {
            if (arrto[i] == arrto[j]) {
                for (int k = 0; k < counter_l; k++) {
                    if (arrto[i] == cachedArr[k]) {
                        key = 0;
                        break;
                    }
                }
                if (key) {
                    cachedArr[counter_l] = arrto[i];
                    counter_l++;
                }
                key = 1;
            }
        }
    }

    cachedArr[counter_l] = arrto[idx1];
    int counter_r = 1 + counter_l;
    int k_ = counter_r - 1;
    for (int i = idx1 + 1; i < len; i++) {
        for (int j = idx1; j < len; j++) {
            if (arrto[i] == arrto[j]) {
                for (int k = k_; k < counter_r; k++) {
                    if (arrto[i] == cachedArr[k]) {
                        key = 0;
                        break;
                    }
                }
                if (key) {
                    cachedArr[counter_r] = arrto[i];
                    counter_r++;
                }
                key = 1;
            }
        }
    }
    printf("(a)");
    printArray(cachedArr, counter_r);
    return counter_r;
}

int findAllMutualElementsAndCopy(float* arrto, float* cachedArr, int len) {
    int iarr_size = 0;
    cachedArr[0] = arrto[0];
    int counter = 1;
    int key = 1;
    for (int i = 1; i < len; i++) {
        for (int j = 0; j < len; j++) {
            if (arrto[i] == arrto[j]) {
                for (int k = 0; k < counter; k++) {
                    if (arrto[i] == cachedArr[k]) {
                        key = 0;
                        break;
                    }
                }
                if (key) {
                    cachedArr[counter] = arrto[i];
                    counter++;
                }
                key = 1;
            }
        }
    }
    printf("(b)");
    printArray(cachedArr, counter);
    return counter;
}

void copyMutualElements_b(float* newArr, float* cachedArr, int iarr_size) {
    for (int i = 0; i < iarr_size; i++) {
        newArr[i] = cachedArr[i];
    }
}