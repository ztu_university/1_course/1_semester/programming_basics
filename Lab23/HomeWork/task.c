#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define buffSize 64
#define Building_t_timeUntilOverhaul 25

typedef struct {
    char address[buffSize];
    char buildingType[buffSize];
    unsigned short floor;
    unsigned int apartments;
    unsigned int exploitationPeriod;
    unsigned int timeUntilOverhaul;
} Building_t;

typedef struct {
    char address[buffSize];
    char buildingType[buffSize];
    unsigned int floor;
    unsigned int apartments;
    unsigned int exploitationPeriod;
    unsigned int timeUntilOverhaul;
} Building_param_t;

void mainMenu();
int selectFunction();
void processUserOption(int userOption, Building_t building[], int size, int key);
int setNumberOfBuildings();
void setBuilding(Building_t building[], int size, int key);
void setTimeUntilOverhaul(Building_t building[], int numberOfBuilding, int* key);
void showBuildingSpecs(Building_t building[], int size, int key);
void chooseFunction(Building_t building[], int size);
int printListOfParamsAndGetOption(int* key);
void chooseParam(Building_t building[], int* value, Building_param_t* buildingParam, int option);
void findBuildingWithParam(Building_t building[], int size, int* param, Building_param_t* buildingParam);
void sortListOfBuildingsWithParam(Building_t building[], int size, int* param);
int cmpFloorInStruct(const void* a, const void* b);
int cmpApartmentsInStruct(const void* a, const void* b);
int cmpExploitationPeriodInStruct(const void* a, const void* b);
int cmpTimeUntilOverhaulInStruct(const void* a, const void* b);
int cmpAddressInStruct( const void *a, const void *b );
int cmpBuildingTypeInStruct( const void *a, const void *b );

int main() {
    mainMenu();
    return 0;
}

void mainMenu() {
    long long int size_building = sizeof(Building_t);

    Building_t* Campus;
    Campus = calloc(1, size_building);

    int exitStatus = 0;
    int n = 1, key = 0;
    long long int diff;
    long long int n_new_size = size_building, 
                  n_old_size = size_building;

    while (!exitStatus) {
        int userOption = selectFunction();
        if (userOption >= 2 && userOption <= 3) {
            printf ("\nFull list of campuses type [-1], "
                    "for exact campus type it's number:\n");
            scanf ("%d", &key);
            processUserOption (userOption, Campus, n, key);

        } else if (userOption == 4) {
            processUserOption (userOption, Campus, n, key);
        } else if (userOption == 1) {
            n_old_size = n * size_building;
            n = setNumberOfBuildings();
            n_new_size = size_building * n;
            if (n <= 0) {
                printf ("Quitting\n");
                free(Campus);
                exitStatus = 1;
            // provide a function to handle all that
            } else if (n > 1) {
                Campus = realloc(Campus, n_new_size);
                if (n_new_size > n_old_size && Campus) {
                    diff = n_new_size - n_old_size;
                    void* p_old = ((char*)Campus) + n_old_size;
                    memset(p_old, 0, diff);             // zeroing new bigger part of memory
                }        
            } // !
        } else {
            printf ("Quitting\n");
            free(Campus);
            exitStatus = 1;
        }
    }
}

int selectFunction() {
    printf ("\nPlease, select any from the list or type 0 to quit\n"
           "1 - Set number of campuses\n"
           "2 - Create campus specifications\n"
           "3 - Get campus specifications\n"
           "4 - Choose parameter to operate with Campuses\n"
           );
    int option = 0;
    scanf ("%d", &option);
    return option;
}

void processUserOption(int userOption, Building_t building[], int numOfBuildings, int key) {

    switch (userOption) {
    case 2:
        setBuilding (building, numOfBuildings, key);
        break;
    case 3:
        showBuildingSpecs (building, numOfBuildings, key);
        break;
    case 4:
        chooseFunction(building, numOfBuildings);
        break;
    }
}

int setNumberOfBuildings() {
    printf ("\nHow many campuses do you want?\n"
            "If you enter the number which is less than it was "
            "(or any negative number),"
            " than the latest campuses will be deleted from the list!\n"
            );
    int number;
    scanf ("%d", &number);
    return number;
}

void setBuilding(Building_t building[], int numberOfBuildings, int key) {
    int counter = 0;
    int key_ = 1;
    int key__ = 1;
    if (key >= 0 && key <= numberOfBuildings) {
        counter = key;
        numberOfBuildings = key + 1;
    } else if (key == -1) {
        key_ = 1;
    } else {
        printf ("Wrong campus number\n");
        key_ = -1;
    } 
    if (key_ == 1) {
        while (counter < numberOfBuildings) {
            printf ("\nSet address of building '%d':\t\t\t", counter);
            scanf("%s", building[counter].address);

            printf ("Set type of building '%d':\t\t\t", counter);
            scanf("%s", building[counter].buildingType);

            printf ("Set number of floors in building '%d':\t\t", counter);
            scanf("%d", &building[counter].floor);

            printf ("Set number of apartments in building '%d':\t", counter);
            scanf("%d", &building[counter].apartments);

            printf ("Set exploitation period of building '%d':\t", counter);
            scanf("%d", &building[counter].exploitationPeriod);

            setTimeUntilOverhaul (building, counter, &key__);

            counter++;
        }
    }
}

void setTimeUntilOverhaul(Building_t building[], int counter, int* key__) {
    if (*key__) {
        printf ("\nTime until overhaul of building is atomatically set to '%d'"
                " - period of exploatiation:\n", Building_t_timeUntilOverhaul);
        *key__ = 0;
    }
    building[counter].timeUntilOverhaul = Building_t_timeUntilOverhaul - building[counter].exploitationPeriod;
}

void showBuildingSpecs(Building_t building[], int numberOfBuildings, int key) {
    int counter = 0;
    int n = 0;
    int key_ = 1;
    if (key >= 0 && key <= numberOfBuildings) {
        counter = key;
        numberOfBuildings = key + 1;
    } else if (key == -1) {
        key_ = 1;
    } else {
        printf ("Wrong campus number\n");
        key_ = -1;
    } 
    if (key_ == 1) {
        while (counter < numberOfBuildings) {
            printf ("\nAddress of building '%d':\t\t", n);
            puts(building[counter].address);

            printf ("Type of building '%d':\t\t\t", n);
            puts(building[counter].buildingType);

            printf ("Number of floors in building '%d':\t", n);
            printf ("%d\n", building[counter].floor);

            printf ("Number of apartments in building '%d':\t", n);
            printf ("%d\n", building[counter].apartments);

            printf ("Exploitation period of building '%d':\t", n);
            printf ("%d\n", building[counter].exploitationPeriod);

            printf ("Time until overhaul of building '%d':\t", n);
            printf ("%d\n", building[counter].timeUntilOverhaul);

            counter++;
            n++;
        }
    }
}

void chooseFunction(Building_t* building, int n) {
    printf ("What do you want to do now?\n"
            "1 - Find campus with parameter\t"
            "2 - Sort list of campuses with parameter\n");
    int option = 0;
    int choosingOption = 1;
    Building_param_t struct_param;
    int param;
    while (choosingOption) {
        scanf ("%d", &option);
        switch (option) {
        case 1:
            chooseParam (building, &param, &struct_param, option);
            findBuildingWithParam (building, n, &param, &struct_param);
            choosingOption = 0;
            break;
        case 2:
            chooseParam (building, &param, &struct_param, option);
            sortListOfBuildingsWithParam (building, n, &param);
            choosingOption = 0;
            break;
        default:
            printf ("You entered wrong option!\nTry again\n");
            choosingOption = 1;
            break;
        }
    }
}

int printListOfParamsAndGetOption(int* key) {
    if (*key) {
        printf ("Set the parameter you want to apply:\n"
                "1 - Address\n"
                "2 - Building type\n"
                "3 - Number of floors\n"
                "4 - Number of apartments\n"
                "5 - Exploitation date\n"
                "6 - Overhaul date\n"
                );
        *key = 0;
    }
    int parameter = 0;
    scanf ("%d", &parameter);
    return parameter;
}

void chooseParam(Building_t* building, int* value, Building_param_t* param, int option_) {
    int option, key = 1, chooseParam = 1;
    int key_ = option_;
    while (chooseParam) {
        option = printListOfParamsAndGetOption(&key);
        switch (option) {
        case 1:
            if (key_ == 1)
                scanf ("%s", (*param).address);
            *value = 1;
            chooseParam = 0;
            break;
        case 2:
            if (key_ == 1)
                scanf ("%s", (*param).buildingType);
            *value = 2;
            chooseParam = 0;
            break;
        case 3:
            if (key_ == 1)
                scanf ("%d", &(*param).floor);
            *value = 3;
            chooseParam = 0;
            break;
        case 4:
            if (key_ == 1)
                scanf ("%d", &(*param).apartments);
            *value = 4;
            chooseParam = 0;
            break;
        case 5:
            if (key_ == 1)
                scanf ("%d", &(*param).exploitationPeriod);
            *value = 5;
            chooseParam = 0;
            break;
        case 6:
            if (key_ == 1)
                scanf ("%d", &(*param).timeUntilOverhaul);
            *value = 6;
            chooseParam = 0;
            break;
        default:
            printf ("You entered wrong option!\nTry again\n");
            chooseParam = 1;
            break;
        }
    }
}

void findBuildingWithParam(Building_t building[], int n, int* param, Building_param_t* bParam) {
    int matches = 0;
    switch (*param) {
    case 1:
        for (int i = 0; i < n; i++) {
            if (strncmp ((*bParam).address, building[i].address, 3) == 0) {
                showBuildingSpecs (building, n, i);
                matches++;
            }
        }
        printf ("%d matches found\n", matches);
        matches = 0;
        break;
    case 2:
        for (int i = 0; i < n; i++) {
            if (strncmp ((*bParam).buildingType, building[i].buildingType, 4) == 0) {
                showBuildingSpecs (building, n, i);
                matches++;
            }
        }
        printf ("%d matches found\n", matches);
        matches = 0;
        break;
    case 3:
        for (int i = 0; i < n; i++) {
            if (building[i].floor == (*bParam).floor) {
                showBuildingSpecs (building, n, i);
                matches++;
            }
        }
        printf ("%d matches found\n", matches);
        matches = 0;
        break;
    case 4:
        for (int i = 0; i < n; i++) {
            if (building[i].apartments == (*bParam).apartments) {
                showBuildingSpecs (building, n, i);
                matches++;
            }
        }
        printf ("%d matches found\n", matches);
        matches = 0;
        break;
    case 5:
        for (int i = 0; i < n; i++) {
            if (building[i].exploitationPeriod == (*bParam).exploitationPeriod) {
                showBuildingSpecs (building, n, i);
                matches++;
            }
        }
        printf ("%d matches found\n", matches);
        matches = 0;
        break;
    case 6:
        for (int i = 0; i < n; i++) {
            if (building[i].buildingType == (*bParam).buildingType) {
                showBuildingSpecs (building, n, i);
                matches++;
            }
        }
        printf ("%d matches found\n", matches);
        matches = 0;
        break;
    default:
        break;
    }
}

void sortListOfBuildingsWithParam(Building_t building[], int n, int* param) {
    int chooseSort = 1;
    while (chooseSort) {
        switch (*param) {
        case 1:
            qsort (building, n, sizeof(Building_t), cmpAddressInStruct);
            chooseSort = 0;
            break;
        case 2:
            qsort (building, n, sizeof(Building_t), cmpBuildingTypeInStruct);
            chooseSort = 0;
            break;
        case 3:
            qsort (building, n, sizeof(Building_t), cmpFloorInStruct);
            chooseSort = 0;
            break;
        case 4:
            qsort (building, n, sizeof(Building_t), cmpApartmentsInStruct);
            chooseSort = 0;
            break;
        case 5:
            qsort (building, n, sizeof(Building_t), cmpExploitationPeriodInStruct);
            chooseSort = 0;
            break;
        case 6:
            qsort (building, n, sizeof(Building_t), cmpTimeUntilOverhaulInStruct);
            chooseSort = 0;
            break;
        default:
            printf ("You entered wrong option!\nTry again\n");
            chooseSort = 1;
            break;
        }
    }
}

int cmpFloorInStruct(const void* a, const void* b) {
    const Building_t *p1 = a;
    const Building_t *p2 = b;
    return ( p1->floor - p2->floor );
}
int cmpApartmentsInStruct(const void* a, const void* b) {
    const Building_t *p1 = a;
    const Building_t *p2 = b;
    return ( p1->apartments - p2->apartments );
}
int cmpExploitationPeriodInStruct(const void* a, const void* b) {
    const Building_t *p1 = a;
    const Building_t *p2 = b;
    return ( p1->exploitationPeriod - p2->exploitationPeriod );
}
int cmpTimeUntilOverhaulInStruct(const void* a, const void* b) {
    const Building_t *p1 = a;
    const Building_t *p2 = b;
    return ( p1->timeUntilOverhaul - p2->timeUntilOverhaul );
}
int cmpAddressInStruct(const void *a, const void *b) {
    const Building_t *p1 = a;
    const Building_t *p2 = b;
    return strcmp( p1->address, p2->address );
}
int cmpBuildingTypeInStruct( const void *a, const void *b ) {
    const Building_t *p1 = a;
    const Building_t *p2 = b;
    return strcmp( p1->buildingType, p2->buildingType );
}