#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

void setArr(int arr[], int size);
void printArray(int arr[], int size);
void copyArr(int arr[], int cpArr[], int size);
void bubbleSort(int arr[], int size);
void selectionSort(int arr[], int size);
void insertionSort(int arr[], int size);
void shellSort(int arr[], int size);

int main() {
    printf ("Set array size\t");
    unsigned long arrSize;
    scanf ("%d", &arrSize);
    int initArr[arrSize];
    int cloneArr[arrSize];

    setArr(initArr, arrSize);
    //printArray(initArr, arrSize);
    copyArr(initArr, cloneArr, arrSize);
    printf ("\n");
    
    bubbleSort(cloneArr, arrSize);
    //printArray(cloneArr, arrSize);
    copyArr(initArr, cloneArr, arrSize);
    selectionSort(cloneArr, arrSize);
    //printArray(cloneArr, arrSize);
    copyArr(initArr, cloneArr, arrSize);
    insertionSort(cloneArr, arrSize);
    //printArray(cloneArr, arrSize);
    copyArr(initArr, cloneArr, arrSize);
    shellSort(cloneArr, arrSize);
    //printArray(cloneArr, arrSize);
    return 0;
}

void setArr(int arr[], int n) {
    srand (time(NULL));
    rand();
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 500;
    }
}

void printArray(int arr[], int n) {
  for (int i = 0; i < n; ++i) {
    printf("%d ", arr[i]);
  }
  printf("\n");
}

void copyArr(int initArr[], int cloneArr[], int n) {
    for (int i = 0; i < n; i++) {
        cloneArr[i] = initArr[i];
    }
}

void bubbleSort(int arr[], int n) {
    float runtime;
    clock_t start, finish;
    
    start = clock();
    int c, d, t;

    for (c = 0 ; c < n - 1; c++) {
        for (d = 0 ; d < n - c - 1; d++) {
            if (arr[d] > arr[d+1]) {
                t = arr[d];
                arr[d] = arr[d+1];
                arr[d+1] = t;
            }
        }
    }
    finish = clock();
    runtime = (float)(finish - start) / CLOCKS_PER_SEC;
    printf ("bubble sort algorithm runtime -\t\t%f\n", runtime);
}

void selectionSort(int arr[], int n) {
    float runtime;
    clock_t start, finish;
    
    start = clock();
    int i, j, min_idx, temp;

    for (i = 0; i < n-1; i++) {
        min_idx = i;
        for (j = i+1; j < n; j++)
            if (arr[j] < arr[min_idx]) {
                min_idx = j;
            }
        temp = arr[i];
        arr[i] = arr[min_idx];
        arr[min_idx] = temp;
    }
    finish = clock();
    runtime = (float)(finish - start) / CLOCKS_PER_SEC;
    printf ("selection sort algorithm runtime -\t%f\n", runtime);
}

void insertionSort(int arr[], int n) {
    float runtime;
    clock_t start, finish;
    
    start = clock();
    int i, key, j;
    for (i = 1; i < n; i++) {
        key = arr[i];
        j = i - 1;

        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
    finish = clock();
    runtime = (float)(finish - start) / CLOCKS_PER_SEC;
    printf ("insertion sort algorithm runtime -\t%f\n", runtime);
}

void shellSort(int arr[], int n) {
    float runtime;
    clock_t start, finish;
    
    start = clock(); 
    for (int gap = n/2; gap > 0; gap /= 2) { 
        for (int i = gap; i < n; i += 1) { 
            int temp = arr[i]; 
            int j;             
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) 
                arr[j] = arr[j - gap]; 
            arr[j] = temp; 
        } 
    } 
    finish = clock();
    runtime = (float)(finish - start) / CLOCKS_PER_SEC;
    printf ("shell sort algorithm runtime -\t\t%f\n", runtime);
}