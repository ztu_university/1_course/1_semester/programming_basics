// More info about heapify() and heapSort() is in HWtask_Info.txt
#include <stdio.h>

void heapSort(int arr[], int size);
void heapify(int arr[], int size, int parentNode);
void printArray(int arr[], int size);

int main() {
    int n = 5;
    int arr[] = {10, -9, 2, 1, 7};
    
    heapSort(arr, n);
    printArray(arr, n);
    return 0;
}

void heapSort(int arr[], int n) {
    // max heap here
    for (int i = n/2 - 1; i >= 0; i--) {
        heapify (arr, n, i);
    }
    // sort here
    int temp;
    for (int i = n - 1; i > 0; i--) {
        temp = arr[0];
        arr[0] = arr[i];
        arr[i] = temp;
        heapify (arr, i, 0);
    }
}

void heapify(int arr[], int n, int parent) {
    //printArray (arr, n);
    int largest = parent;
    int lchild = 2*parent + 1;
    int rchild = 2*parent + 2;
    int temp;

    if (lchild < n && arr[largest] < arr[lchild]) {
        largest = lchild;
    }
    if (rchild < n && arr[largest] < arr[rchild]) {
        largest = rchild;
    }

    if (largest != parent) {
        temp = arr[parent];
        arr[parent] = arr[largest];
        arr[largest] = temp;
        //printf ("Swapped\n");
        heapify (arr, n, largest);
    }
    //printf ("Leave func\n");
    //printArray (arr, n);
}

void printArray(int arr[], int n) {
  for (int i = 0; i < n; ++i) {
    printf("%d ", arr[i]);
  }
  printf("\n");
}