#include <stdio.h>

int main() {
    int i = 4;
    int j = 3;
    int c[i][j];

    for (int a = 0; a < i; a++) {
        for (int b = 0; b < j; b++) {
            c[a][b] = 4*a - 2*b;
            printf ("%3d ", c[a][b]);
        }
        printf ("\n");
    }
     
    int sum = 0;
    for (int a = 0; a < j; a++) {
        sum += c[i-1][a];
    }
    printf ("Sum = %d", sum);
    return 0;
}