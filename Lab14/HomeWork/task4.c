#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    printf ("Set length of square:\t");
    int n;
    scanf ("%d", &n);
    float arr[n][n];

    srand(time(NULL));
    rand();
    for (int a = 0; a < n; a++) {
        for (int b = 0; b < n; b++) {
            arr[a][b] = ((rand() % 2000) - 1000) / 100.0f;
            printf ("%5.2f ", arr[a][b]);
        }
        printf ("\n\n");
    }

    float sum = 0;
    int ab = n - 1;
    float min = arr[n-1][0];
    float max = arr[n-1][0];
    for (int a = 0; a < n/2; a++) {
        for (int b = ab; b >= n/2; b--) {
            if (min > arr[b][a])
                min = arr[b][a];
            if (max < arr[b][a])
                max = arr[b][a];
        }
        ab--;
    }
    for (int a = n/2; a < n; a++) {
        for (int b = n/2; b < n; b++) {
            if (a <= b) {
                if (min > arr[a][b])
                    min = arr[a][b];
                if (max < arr[a][b])
                    max = arr[a][b];
            }
        }
    }
    sum = min + max;
    printf ("min + max = %5.2f\n", sum);
    return 0;
}