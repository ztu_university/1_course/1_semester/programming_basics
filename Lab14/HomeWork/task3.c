#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const int N = 50;
const int M = 50;

int main() {
    float arr[N][M];
    printf ("Set length (n <= 50) and height (m <= 50) of matrix:\t");
    int n, m;
    scanf ("%d %d", &n, &m);
    if (n <= N && m <= M && n >= 0 && m >= 0) {
        printf ("Set range of values to generate:\t");
        int a, b;
        scanf ("%d %d", &a, &b);
        if (a > b) {
            int temp = a;
            a = b;
            b = temp;
        }
        srand(time(NULL));
        rand();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arr[i][j] = (rand() / (float)RAND_MAX) * (b - a) + a;
                printf ("%8f ", arr[i][j]);
            }
            printf ("\n\n");
        }
        float newArr[m];
        float sum = 0.0f;
        for (int row = 0; row < m; row++) {
            for (int col = 0; col < n; col++) {
                sum += arr[col][row];
            }
            newArr[row] = sum;
            sum = 0.0f;
        }
        printf ("\nNew array:\n");
        for (int j = 0; j < m; j++) {
            printf ("%8f ", newArr[j]);
        }
    }

    return 0;
}