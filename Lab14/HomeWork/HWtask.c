#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    const int k = 6;
    const int n = 5;
    int A[k][n];
    srand(time(NULL));
    rand();
    printf ("%6c  1\t2\t3\t4\t5\n"
    "%-8c----------------------------------\n", 110, 107);
    for (int j = 0; j < k; j++) {
        printf ("%-7d| ", j + 1);
        for (int i = 0; i < n; i++) {
            A[j][i] = rand() % (10 * 14) + 50;
            printf ("%d\t", A[j][i]);
        }
        printf ("\n%8c\n", 124);
    }
    int kCount = 0;
    for (int j = 0; j < n; j++) {
        for (int i = 0; i < k; i++) {
            if (j == 0) {
                if (A[i][j] > 100 && A[i][4] > 100) {
                    kCount++;
                }
            }
        }
    }
    int n1_n5_more_than_100_k[kCount];
    if (kCount != 0) {
        for (int i = 0, j = 0; i < k; i++) {
            if (A[i][0] > 100 && A[i][4] > 100) {
                n1_n5_more_than_100_k[j] = i + 1;
                printf ("In settlement (k) %d\n", n1_n5_more_than_100_k[j]);
                j++;
            }
        }
    }
    else
        printf ("Seems like both don't have more than 100 votes together!\n");

    return 0;
}
