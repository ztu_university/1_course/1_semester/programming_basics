#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    int n, m;
    n = 5;
    m = n;
    float b[n][m];

    srand(time(NULL));
    rand();
    for (int a = 0; a < n; a++) {
        for (int c = 0; c < m; c++) {
            b[a][c] = ((rand() % 2000) - 1000) / 100.0f;
        }
    }
     
    for (int a = 0; a < n; a++) {
        for (int c = 0; c < m; c++) {
            printf ("%5.2f ", b[a][c]);
        }
        printf ("\n\n");
    }
    float min = b[0][0];
    float max = b[0][0];
    int idx_min_x, idx_max_x;
    int idx_min_y, idx_max_y;
    float sum = 0.0f;
    float prod = 1.0f;
    float diagSum = 0.0f;
    float diagSum_ = 0.0f;
    for (int a = 0; a < n; a++) {
        for (int c = 0; c < m; c++) {
            if (min > b[a][c]) {
                min = b[a][c];
                idx_min_x = a;
                idx_min_y = c;
            }
            if (max < b[a][c]) {
                max = b[a][c];
                idx_max_x = a;
                idx_max_y = c;
            }
            sum += b[a][c];
            if (c < a) {
                diagSum_ += b[a][c];
            }
            
        }
        printf ("Sum of %d row = %5.2f\n", a, sum / n);
        sum = 0.0f;
        prod *= b[a][a];
        diagSum += b[a][a];
    }
    printf ("Product of main diagonal =\t\t%5.2f\n", prod);
    printf ("Sum of main diagonal =\t\t\t%5.2f\n", diagSum);
    printf ("Sum of elements below diagonal =\t%5.2f\n", diagSum_);
    printf ("\nMin = %.2f\t\tMax = %.2f\nidx_min = [%d][%d]\tidx_max = [%d][%d]",
            min, max, idx_min_x, idx_min_y, idx_max_x, idx_max_y);
    return 0;
}