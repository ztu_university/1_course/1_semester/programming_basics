#include "stdio.h"

int main(void)
{
  printf ("\nSet numbers {x, y, z}\n");
  double x, y, z;
  scanf ("%lf %lf %lf", &x, &y, &z);

  double expr1 = x * x + y * y;
  double expr2 = y * y + z * z;
  double res;

  if (expr1 < expr2)
  {
    printf ("x*x + y*y < y*y + z*z -> %lf*%lf + %lf*%lf = min", x, x, y, y);
    res = expr1 / 4.0;
    printf ("\n%lf", res);
  }
  else if (expr1 > expr2)
  {
    printf ("x*x + y*y > y*y + z*z -> %lf*%lf + %lf*%lf = min", y, y, z, z);
    res = expr2 / 4.0;
    printf ("\n%lf", res);
  }
  else
  {
    res = expr1 / 4.0;
    printf ("x*x + y*y = y*y + z*z -> both expressions are minimal\n%lf", res);
  }
  return 0;
}
