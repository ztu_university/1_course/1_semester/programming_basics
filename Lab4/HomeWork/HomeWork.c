#include "stdio.h"
#define _USE_MATH_DEFINES // M_E
#include "math.h"


int main(void)
{
  // x^2 + y^2 <= 16;     
  // y <= -1 - e^x / 4;   x = -3.8716 and y = -1.0052  x = 2.2285 and y = -3.3216
  // y >= 1 + e^x / 4;    x = -3.8716 and y = 1.0052   x = 2.2285 and y = 3.3216
  

  printf ("Set {x, y}\n");
  double x, y;
  scanf ("%lf %lf", &x, &y);

  double expr0 = x*x + y*y;
  double expr1 = 1.0 + (pow(M_E, x) / 4.0) + y;
  double expr2 = 1.0 + (pow(M_E, x) / 4.0) - y;

  if ((expr0 <= 16.0) && ((0.0 >= expr1) || (0.0 <= expr2)))
    printf ("\ntrue");
  else
    printf ("\nfalse"); 
  
  return 0;
}
