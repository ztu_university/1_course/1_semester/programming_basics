#include "stdio.h"
#include "math.h"
#include "windows.h"
typedef double Parameter_t;
typedef double Discriminant_t;
typedef double Root_t;

int main(void)
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);

  printf ("Ax^2 + Bx + C = 0 equation solving"
          " on the set of real numbers\n"
          "\nSet A:\t");
  Parameter_t A;
  scanf ("%lf", &A);

  printf ("\nSet B:\t");
  Parameter_t B;
  scanf ("%lf", &B);

  printf ("\nSet C:\t");
  Parameter_t C;
  scanf ("%lf", &C);

  Discriminant_t D;
  if (A != 0.0)
    D = B * B - 4.0 * A * C;
  else if (A == 0.0 && B != 0)
  {
    Root_t X_Linear = (-C) / B;
    printf ("\nIt's linear equation %lfx + (%lf) = 0\nX = %lf", B, C, X_Linear);
    return 0;
  }
  else if (A == 0 && B == 0 && C == 0)
  {
    printf ("\nTrue");
    return 0;
  }

  if (D < 0.0)
  {
    printf ("\nNo real roots, discriminant %lf is < 0", D);
  }
  else if (D == 0.0)
  {
    Root_t X = -B / 2.0 / A;
    printf ("\nDiscriminant = %.lf\nReal root X = %lf", D, X);
  }
  else
  {
    Root_t X_1 = (-B / 2.0 / A) + (sqrt(D) / 2.0 / A);
    Root_t X_2 = (-B / 2.0 / A) - (sqrt(D) / 2.0 / A);
    printf ("\nDiscriminant = %.lf\nReal roots X_1 = %lf\tX_2 = %lf", D, X_1, X_2);
  }
  return 0;
}
