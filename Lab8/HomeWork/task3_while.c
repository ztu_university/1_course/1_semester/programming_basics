#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>
const float e = M_E;

int main (void) {
    float func;
    float i = 5;
    while (i <= 9) {
        func  = sin(i) / (1 + cos(i));
        printf ("The value of function [1] at %.f = %6.3f\n", i, func); 
        i += 2;
    }
    printf ("\n");
    i = 1.0f;
    while (i <= 2.4f) {
        func  = sin(i*i) - pow(e, i);
        printf ("The value of function [2] at %.1f = %6.3f\n", i, func); 
        i += 0.2f;
    }
    return 0;
}