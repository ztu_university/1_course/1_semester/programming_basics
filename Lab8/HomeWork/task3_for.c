#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>
const float e = M_E;

int main (void) {
    float func;
    for (int i = 5; i <= 9; i += 2) {
        func  = sin(i) / (1 + cos(i));
        printf ("The value of function [1] at %d = %6.3f\n", i, func); 
    }
    printf ("\n");
    for (float i = 1.0f; i <= 2.4f; i += 0.2f) {
        func  = sin(i*i) - pow(e, i);
        printf ("The value of function [2] at %.1f = %6.3f\n", i, func); 
    }

    return 0;
}