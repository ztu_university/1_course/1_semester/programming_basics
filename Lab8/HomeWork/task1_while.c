#include <stdio.h>

int main (void) {
    printf ("Set price\n");
    float price;
    scanf ("%f", &price);

    if (price > 0.0f) {
        int i = 1;
        while (i <= 10) {
            printf ("%d kilos of candies costs %.2f\n", i, price * i);
            i++;
        }
    }
    
    return 0;
}