#include <stdio.h>

int main (void) {
    printf ("Set price\n");
    float price;
    scanf ("%f", &price);

    if (price > 0.0f) {
        for (int i = 1; i <= 10; i++) {
            printf ("%d kilos of candies costs %.2f\n", i, price * i);
        }
    }
    
    return 0;
}