#include <stdio.h>

int main (void) {
    unsigned int func;              // including math.h for abs() wasn't provided
    int isPrime = 1;                // assumed if number in range 0-3 it is prime
    for (int i = 0; i <= 15; i++)   // 16 numbers to check
    {
        func = i*i + i + 17;
        for (int j = 2; j <= func/2; j++)
        {
            if (func % j == 0) {
                isPrime = 0;        // 
                break;              // if number has at least 1 divisor -> quit cycle
            } else {
                isPrime = 1;        // no quitting as we have to check all possible divisors
            }
        }
        if (isPrime == 1) {
            printf ("%d. %d\t\tprime\n", i + 1, func);
        } else {
            printf ("%d. %d\t\tcomposite\n", i + 1, func);
        }
    }
    return 0;
}