#include <stdio.h>

int main (void) {
    printf ("Set number\tnumber > 2\n");
    unsigned int n;
    scanf ("%d", &n);

    double res = 1;
    if (n > 2) {
        for (int i = 2; i <= n; i++) {
            res *= (1.0 - 1.0/i/i);
        }
        printf ("Result = %lf\n", res);
    }
    
    return 0;
}