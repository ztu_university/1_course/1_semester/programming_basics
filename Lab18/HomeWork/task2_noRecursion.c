#include <stdio.h>
#include <math.h>

int main() {
    int n = 5;
    float sum = sqrt(n);
    for (int i = n - 1; i >= 1; i--) {
        sum = sqrt(i + sum);
    }
    printf("\nSum = %f\n", sum);
    return 0;
}