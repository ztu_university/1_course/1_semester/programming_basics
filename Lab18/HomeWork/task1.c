#include <stdio.h>

float calcSum(float arr[], int n) {
    float sum = arr[n - 1];
    if (n > 0) {
        sum += calcSum (arr, n - 1);
    }
    return sum;
}

int main() {
    float arr[] = {3.14159f, 2.71828f, 1.41421f, 1.61803f, 1.73205f};
    int size = sizeof(arr) / sizeof(arr[0]);
    float sum = 0.0f;
    sum = calcSum(arr, size);
    printf("\nSum = %f\n", sum);
    return 0;
}