#include <stdio.h>
#include <math.h>

float calcSum(int n, int temp) {
    float sum = temp;
    if (temp < n) {
        sum += calcSum(n, temp + 1);
    }
    return sqrt(sum);
}

int main() {
    int n = 5;
    float sum = 0.0f;
    sum = calcSum(n, 1);
    printf("\nSum = %f\n", sum);
    return 0;
}