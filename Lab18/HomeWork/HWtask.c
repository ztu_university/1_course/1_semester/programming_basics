#include <stdio.h>
#include <math.h>

int calcSum(int n) {
    int sum = 1;
    if (n >= 10) {
        sum += calcSum(n/10);
    } else if (n < -9) {
        sum += calcSum(n/10);
    }
    return sum;
}

int main() {
    int n = 123456;
    int sum;
    sum = calcSum(n);
    printf("\nSum = %d\n", sum);
    return 0;
}