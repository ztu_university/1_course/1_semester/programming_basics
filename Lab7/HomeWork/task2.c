#include "stdio.h"

int main(void)
{
  printf ("Set x:\t");
  float x;
  scanf ("%f", &x);

  float y = (x > 5 ? (x + 1) : (x + 4));
  printf ("\ny(x) = %.2f", y);
  return 0;
}
