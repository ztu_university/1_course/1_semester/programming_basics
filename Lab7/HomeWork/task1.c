#include "stdio.h"

int main()
{
  printf ("Set x and y:\t");
  float x, y;
  scanf ("%f %f", &x, &y);
  // y = x + 1; y = -x + 1;

  if ((x <= 0.0f && ((y + x - 1) <= 0.0f) && ((y - x - 1) >= 0.0f)) ||
      (x >= 0.0f && ((y + x - 1) >= 0.0f) && ((y - x - 1) <= 0.0f)))
    printf ("true\t\tpoint {%.2f; %.2f} belongs to that area!", x, y);
  else
    printf ("false\t\tpoint {%.2f; %.2f} does not belong to that area!", x, y);

  return 0;
}
