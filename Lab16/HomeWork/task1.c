#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>
const float pi = (float)M_PI;

float calcCircleLen(float radius);

int main() {
    printf ("Set radius of a circle:\t");
    float r;
    scanf ("%f", &r);
    if (r < 0) {
        printf ("Error size\nProgramm terminated\n");
        return 1;
    }
    float circleLen = calcCircleLen(r);
    printf ("\nCircle length = %f\n", circleLen);
    return 0;
}

float calcCircleLen(float r) {
    float circleLen;
    circleLen = 2.0f * pi * r;
    return circleLen;
}