#include <stdio.h>

int reverseNum(int num) {
    int reversedNum = 0;
    while (num != 0) {
        int remainder = num % 10;
        reversedNum = reversedNum * 10 + remainder;
        num /= 10;
    }
    return reversedNum;
}

int main() {
    printf ("Set number:\t");
    int num;
    scanf ("%d", &num);

    printf ("\nReversed number is %d\n", reverseNum(num));
    return 0;
}