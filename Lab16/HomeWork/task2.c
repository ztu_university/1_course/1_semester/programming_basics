#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void setArr(float arr[], int size);
float calcSumOfElements(float arr[], int size);
float findMaxElement(float arr[], int size);
float findMinElement(float arr[], int size);
float calcProdOfElements(float arr[], int size);
void printArr(float arr[], int size);

int main() {
    printf ("Set size of array:\t");
    int n;
    scanf ("%d", &n);
    if (n <= 0) {
        printf ("Error size\nProgramm terminated\n");
        return 1;
    }
    float arr[n];
    setArr (arr, n);
    printArr(arr, n);
    float sum = calcSumOfElements(arr, n);
    float maxEl = findMaxElement(arr, n);
    float minEl = findMinElement(arr, n);
    float prod = calcProdOfElements(arr, n);
    printf ("\nSum of elements =\t%f"
            "\nMaximal element =\t%f"
            "\nMinimal element =\t%f"
            "\nProduct of elements =\t%f\n",
            sum, maxEl, minEl, prod);
    return 0;
}

float findMaxElement(float arr[], int n) {
    float max = arr[0];
    for (int i = 0; i < n; i++) {
        if (max < arr[i]) {
            max = arr[i];
        }
    }
    return max;
}

float findMinElement(float arr[], int n) {
    float min = arr[0];
    for (int i = 0; i < n; i++) {
        if (min > arr[i]) {
            min = arr[i];
        }
    }
    return min;
}

void setArr(float arr[], int n) {
    srand(time(0));
    rand();
    for (int i = 0; i < n; i++) {
        arr[i] = (rand() / (float)RAND_MAX) * 20.0f - 10.0f;
    }
}

float calcProdOfElements(float arr[], int n) {
    float prod = 1.0f;
    for (int i = 0; i < n; i++) {
        prod *= arr[i];
    }
    return prod;
}

float calcSumOfElements(float arr[], int n) {
    float sum = 0.0f;
    for (int i = 0; i < n; i++) {
        sum += arr[i];
    }
    return sum;
}

void printArr(float arr[], int n) {
    for (int i = 0; i < n; i++) {
        if (i % 4 == 0) {
            printf ("\n\n");
        }
        printf ("%f ", arr[i]);
    }
    printf ("\n\n");
}