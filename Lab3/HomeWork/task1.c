#include "stdio.h"
#include "windows.h"

int main(void)
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);

  printf ("|%-d|\n|%-8d|\n|%8d|\n|%-8d|\n", 555, 666, 555, 555);
  return 0;
}
