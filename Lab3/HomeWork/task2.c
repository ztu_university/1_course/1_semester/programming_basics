#include "stdio.h"
#include "windows.h"
#include "math.h"

int main(void)
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);

  double initValue = 48.17;
  double fraction, integer;
  fraction = modf(initValue, &integer);
  
  printf ("%.f ���� ��� ������ � %.f ���������, %.f ���� � ��� � ���� ���������\n", integer, fraction * 100.0, fraction * 100.0);
  return 0;
}