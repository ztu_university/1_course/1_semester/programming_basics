#include "stdio.h"
#include "windows.h"

int main(void)
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);

  int m = 3, n = 5;
  printf ("\nm = %d\tn = %d\n", m, n);
  int y = m+--n;
  printf ("\n1) m+--n = %d\nm = %d\tn = %d\n", y, m, n);

  y = m++<n++;
  printf ("\n2) m++<n++ = %d\nm = %d\tn = %d\n", y, m, n);

  y = n--<--m;
  printf ("\n3) n--<--m = %d\nm = %d\tn = %d\n", y, m, n);

  y = --n-m;
  printf ("\n4) --n-m = %d\nm = %d\tn = %d\n", y, m, n);

  y = m--<++n;
  printf ("\n5) m--<++n = %d\nm = %d\tn = %d\n", y, m, n);

  y = n++>m;
  printf ("\n6) n++>m = %d\nm = %d\tn = %d\n", y, m, n);
  return 0;
}