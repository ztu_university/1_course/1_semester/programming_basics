#include "stdio.h"
#include "math.h"
#include "windows.h"

int main(void)
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);

  // float-type precision is 7, it ignores digits after 1e-7 precision
  float aF = 1000.0f, bF = 0.0001f;
  float cubicPartF = pow((aF + bF), 3.0f);
  float bracketsPartF = (pow(aF, 3.0f) + 3.0f * pow(aF, 2.0f) * bF);
  float numeratorF = cubicPartF - bracketsPartF;
  float denominatorF = 3.0f * aF * pow(bF, 2.0f) + pow(bF, 3.0f);
  float resF = numeratorF / denominatorF;

  printf ("float"
          "\ncubicPartF\t=\t%.7f"
          "\nbracketsPartF\t=\t%.7lf",
          cubicPartF, bracketsPartF);

  printf ("\nnumeratorF\t=\t%.7f"
          "\ndenominatorF\t=\t%.7f",
          numeratorF, denominatorF);

  printf ("\nresF\t\t=\t%.7f", resF);

  double aD = 1000.0, bD = 0.0001;
  double cubicPartD = pow((aD + bD), 3.0);
  double bracketsPartD = (pow(aD, 3.0) + 3.0 * pow(aD, 2.0) * bD);
  double numeratorD = cubicPartD - bracketsPartD;
  double denominatorD = 3.0 * aD * pow(bD, 2.0) + pow(bD, 3.0);
  double resD = numeratorD / denominatorD;

  printf ("\n\ndouble"
          "\ncubicPartD\t=\t%.17lf"
          "\nbracketsPartD\t=\t%.17lf",
          cubicPartD, bracketsPartD);

  printf ("\nnumeratorD\t=\t%.17lf"
          "\ndenominatorD\t=\t%.17lf",
          numeratorD, denominatorD);

  printf ("\nresD\t\t=\t%.17lf", resD);

  return 0;
}