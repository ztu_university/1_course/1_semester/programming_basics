#include "stdio.h"
#define _USE_MATH_DEFINES   // pi
#include "math.h"           // pow(), fabs(), sin()
#include "windows.h"
const double pi = M_PI;

int main(void)
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);  
  
  {
    double a, b, c, d;
    scanf  ("%lf %lf %lf %lf", &a, &b, &c, &d);
    printf ("\nBefore swap:"
            "\na = %lf\nb = %lf\nc = %lf"
            "\nd = %lf", a, b, c, d);

    // a ^= d ^= a ^= d;
    // swaping method for non-integral types
    // a d
    a += d;
    printf ("\nAfter swap:"
            "\ntemp a = %lf", a);
    d = a - d;
    printf ("\nswap d = %lf", d);
    a -= d;
    printf ("\nswap a = %lf\n", a);
    // b c
    c += b;
    printf ("\ntemp c = %lf", c);
    b = c - b;
    printf ("\nswap b = %lf", b);
    c -= b;
    printf ("\nswap c = %lf\n", c);
    // d c
    d += c;
    printf ("\ntemp d = %lf", d);
    c = d - c;
    printf ("\nswap c = %lf", c);
    d -= c;
    printf ("\nswap d = %lf\n", d);
  }
  {
    double alpha;
    scanf ("%lf", &alpha);
    printf ("alpha in degrees = %lf\n", alpha);
    printf ("alpha in radians = %lf", pi / 180 * alpha);
  }
  return 0;
}