#include "stdio.h"
#include "math.h"     // pow(), fabs(), sin()
#include "windows.h"

int main(void)
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);  
  
  double x, y, z;
  scanf("%lf %lf %lf", &x, &y, &z);
  double s = pow(y, x+1.0) / (pow(fabs(y-2.0), 1.0/3.0) + 3.0) + 
                (x + y/2.0) / 2.0 / fabs(x+y) * pow(x+1.0, -1.0/sin(z));

  printf("%.20lf", s);
  return 0;
}