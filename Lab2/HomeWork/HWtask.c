#include "stdio.h"
#include "windows.h"
#define _USE_MATH_DEFINES   // pi
#include "math.h"           // sqrt(), pow(), sin(), cos()


const double rad = 180.0 / M_PI;        // for asin()
const double degree = M_PI / 180.0;     // for sin(), cos()

int main(void)
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);  

  {
    double h, alpha, beta;
    printf ("\n�������� 1"
            "\nSet:\nh =\t\t");
    scanf  ("%lf", &h);
    printf ("\nalpha angle =\t");
    scanf  ("%lf", &alpha);
    printf ("\nbeta angle =\t");
    scanf  ("%lf", &beta);

    if (h > 0.0          &&
        0.0 < alpha      &&
        180.0 > alpha    &&
        0.0 < beta       &&
        180.0 > beta     &&
        180.0 > (alpha + beta))
    {
      double a = h / sin(beta * degree);
      double b = h / sin(alpha * degree);
      double gamma = 180.0 - alpha - beta;
      double c = sqrt(pow(a, 2.0) + pow(b, 2.0) - 2.0 * a * b * cos(gamma * degree));
      double S = h * c / 2.0;
      double P = a + b + c;
      
      printf ("\na =    \t\t%lf \nb =    \t\t%lf"
              "\nc =    \t\t%lf \nalpha =\t\t%lf"
              "\nbeta = \t\t%lf \ngamma =\t\t%lf"
              "\nh =    \t\t%lf \nP =    \t\t%lf"
              "\nS =    \t\t%lf \n",
              a, b, c, alpha, beta, gamma, h, P, S);
    }
    else
    {
      printf ("\nWrong input"
              "\nh =\t\t%lf"
              "\nalpha =\t\t%lf"
              "\nbeta = \t\t%lf"
              "\nalpha + beta =\t%lf"
              "\nHeight can't be less than 0"
              "\nAngles can't be less than 0 and over than 180 degrees"
              "\nSum of alpha and beta in triangle must be less than 180 degrees",
              h, alpha, beta, alpha + beta);
    }
  }
  {
    double S, h, alpha;
    printf ("\n�������� 2"
            "\nSet:\nS =\t\t");
    scanf  ("%lf", &S);
    printf ("\nh =\t\t");
    scanf  ("%lf", &h);
    printf ("\nalpha angle =\t");
    scanf  ("%lf", &alpha);

    if (0.0 < S     &&
        0.0 < h     &&
        0.0 < alpha &&
        180.0 > alpha)
    {
      double c = 2.0 * S / h;
      double b = h / sin(alpha * degree);
      double a, beta;
      if (alpha < 90)         // isosceles triangle
      {
        a = b;         // equal sides except of base
        beta = alpha;  // equal base angles
      }
      else
      {
        a = sqrt(pow(c, 2.0) + pow(b, 2.0) - 2.0 * c * b * cos(alpha * degree));
        beta = asin(h / a) * rad;
      }
      double gamma = 180.0 - alpha - beta;
      double P = a + b + c;

      printf ("\na =    \t\t%lf \nb =    \t\t%lf"
              "\nc =    \t\t%lf \nalpha =\t\t%lf"
              "\nbeta = \t\t%lf \ngamma =\t\t%lf"
              "\nh =    \t\t%lf \nP =    \t\t%lf"
              "\nS =    \t\t%lf \n",
              a, b, c, alpha, beta, gamma, h, P, S);
    }
    else
    {
      printf ("\nWrong input"
              "\nArea (S) =\t%lf"
              "\nHeight = \t\t%lf"
              "\nalpha =\t%lf"
              "\nArea can't be less than 0"
              "\nHeight can't be less than 0"
              "\nAngles can't be less than 0 and over than 180 degrees",
              S, h, alpha);
    }
  }
  return 0;
}