#include <stdio.h>

void foo(int* arr, int n) {
    int temp;
    for (int i = 0; i < n - 1; i += 2) {
        temp = arr[i];
        arr[i] = arr[i + 1];
        arr[i + 1] = temp;
    }
}

int main() {
    int iarr[] = {0, 1, 2, 3, 4, 9, 8, 7, 6}; // 10
    int* arr = iarr;
    // if varialbe-length array is a parameter sizeof counts only it's pointer size
    int size = sizeof(iarr) / sizeof(iarr[0]);
    
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
    // iarr would be a pointer and size would be a pointer size
    foo(arr, size);
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
    return 0;
}