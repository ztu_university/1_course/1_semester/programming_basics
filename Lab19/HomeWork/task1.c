#include <stdio.h>

int main() {
    int* p;         // 1
    int x = 3, y = 7, m[] = {1, 0, 2, 0, 3};    // 2
    p = &y; // 3
    printf("y = %d\t\t\t(4)\n", *p); // 4
    x = *p; printf("x = *p, x = %d\t\t(5)\n", x); // 5
    *p = *p + 7; printf("*p += 7, y = %d\t\t(6)\n", *p); // 6
    printf("p = %d\t\t(7)\n", p); // 7
    *p += 5; printf("y = %d\t\t\t(8)\n", *p); // 8
    return 0;
}
