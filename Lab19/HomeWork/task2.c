#include <stdio.h>

int main() {
    int m[] = {1, 2, 3, 4, 5};
    int m_size = sizeof(m);
    printf ("m_size = %d\n", m_size);                   // 1
    int m_numOfElements = m_size / sizeof(m[0]);
    printf ("m_numOfElements = %d\n", m_numOfElements); // 2
    printf ("first el addr = %p\nlast el addr =  %p\n",
            &m[0], &m[m_numOfElements - 1]);            // 3

    // 4
    int temp;
    for (int i = 0; i < m_numOfElements / 2; i++) {
        temp = m[i];
        m[i] = m[m_numOfElements - 1 - i];
        m[m_numOfElements - 1 - i] = temp;
    }
    for (int i = 0; i < m_numOfElements; i++)
        printf ("m[%d] = %d\n", i, m[i]);
    // !4

    return 0;
}
