#include <stdio.h>
#include <stdlib.h>
#include <time.h>
const int padding = 3;

void printArr(float arr[], int n, int pad) {
    for (int j = pad; j < n - 1; j++) {
        printf("%.2f, ", arr[j]);
    }
        printf("%.2f\n", arr[n - 1]);
}

void setArr(float* arr, int n, int n_) {
    srand (time(NULL));
    rand();
    *(arr + 0)  = 0;       // not necessary
    *(arr + 1)  = 0;       // not necessary
    int j = n_ - n;         //  x x (x)   a a a a ... a
    for (j; j < n_; j++) {  // |padding| |real elements|
        *(arr + j) = (float)rand() / 100.0f;
    }
}

int checkCond(int* pad_, int realSize, int* totalSize) {
    int key = -1;
    if (realSize > 2 && realSize % 2 == 1) {
        *pad_ = padding - 1;
        *totalSize = realSize + *pad_;
        key = 1;
    } else if (realSize > 1 && realSize % 2 == 0) {
        *totalSize = realSize + padding;
        key = 0;
    } else if (realSize == 1) {
        printf("Can't operate with that array\n");
        return key;
    }
    return key;
}

void calcParams(int totalSize, int realSize, int* parr) {
    int mirror_el = totalSize - 1;
    int l_bound = mirror_el / 2 + 1;
    int pad = totalSize - realSize;
    *(parr + 0) = pad;
    *(parr + 1) = mirror_el;
    *(parr + 2) = l_bound;
}

void reverseArr(float* arr, int n, int n_, int key, float* delEl) {
    int paramArr[3];
    calcParams(n_, n, paramArr);
    int pad     = paramArr[0];
    int m       = paramArr[1];
    int l_bound = paramArr[2];
    float temp;
    if (key == 1) {
        setArr(arr, n, n_);     // set last n elements
        printArr(arr, n_, pad);
        // deleting odd element
        int i = (n_ + 1) / 2;
        *delEl = arr[i];
        for (i; i > pad; i--) {
            *(arr + i) = *(arr + i - 1); 
        }
        *(arr + pad) = 0;       // not necessary
        printArr(arr, n_, pad);
        // reverse if odd
        for (int j = pad + 1; j <= l_bound; j++, m--) {
            temp = *(arr + j);
            *(arr + j) = *(arr + m);
            *(arr + m) = temp;
        }
        printArr(arr, n_, pad + 1);
    } else {
        *(arr + 2) = 0;         // not necessary
        setArr(arr, n, n_);     // set last n elements
        printArr(arr, n_, pad);
        // reverse if even
        for (int j = pad; j <= l_bound; j++, m--) {
            temp = *(arr + j);
            *(arr + j) = *(arr + m);
            *(arr + m) = temp;
        }
        printArr(arr, n_, pad);
    }
}

int main() {
    printf ("Set arr size:\t");
    int n;
    scanf ("%d", &n);
    int n_ = 0;
    int pad_ = padding;
    int res = checkCond(&pad_, n, &n_);
    float m[n_];
    float delEl;
    if (res == 0) {
        reverseArr(m, n, n_, res, &delEl);
        printf ("No odd elements in array were deleted\n");
        delEl = -1;
    } else if (res == 1) {
        reverseArr(m, n, n_, res, &delEl);
        printf ("Odd element (%.2f) in array were deleted\n", delEl);
    } else {
        printf ("Error occured\n");
        return -1;
    }
    printf ("Set i 3 times for arr[i + 10]\nAttention:"
            " i must be => -10 and < %d:\t", n - 10);
    int i, i_res;
    for (int b = 2; b >= 0; b--) {
        scanf ("%d", &i);
        i_res = i + 10 + pad_;
        if (i_res < n_ && i_res >= 0) {
            m[b] = m[i_res] - 2;
        } else {
            printf ("%d element has garbage value because you set wrong i\n", b + 1);
        }
    }
    printf("\nYour new array:\n");
    printArr(m, n_, 0);
    return 0;
}