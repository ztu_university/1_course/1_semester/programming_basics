#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

int main(void) {
    int option;
    int isOpen = 1;
    srand(time(NULL));
    rand();             // to prevent first number in low range
    float randNum_f;
    int randNum_n;
    while (isOpen == 1) {
        printf ("Choose generator of random numbers from 1 to 10\n");
        scanf ("%d", &option);
        switch (option)
        {
        case 1: {
            randNum_f = (rand() / (float)RAND_MAX) * (-0.999f - -4.0f) + -4.0f;
            printf ("Random generated number:\t%f\n", randNum_f);
            break;
        }
        case 2: {
            randNum_n = rand() % (299 - 100) + 100;
            printf ("Random generated number:\t%d\n", randNum_n);
            break;
        }
        case 3: {
            randNum_n = rand() % (-1 - -35) + -35;
            if (randNum_n % 2 == 0) 
                printf ("Random generated number:\t%d\n", randNum_n);
            else
                printf ("Number is odd \n");
            break;
        }
        case 4: {
            randNum_n = rand() % (127 - -128) + -128;
            printf ("Random generated number:\t%d\n", randNum_n);
            break;
        }
        case 5: {
            randNum_n = rand() % (12 - -7) + -7;
            if (randNum_n % 2 != 0)
                printf ("Random generated number:\t%d\n", randNum_n);
            else
                printf ("Number is even \n");
            break;
        }
        case 6: {
            randNum_f = (rand() / (float)RAND_MAX) * (28 * sqrt(3) - -7.85f) + -7.85f;
            printf ("Random generated number:\t%f\n", randNum_f);
            break;
        }
        case 7: {
            randNum_n = rand() % (100 - -100) + -100;
            printf ("Random generated number:\t%d\n", randNum_n);
            break;
        }
        case 8: {
            randNum_n = rand() % (71 - 23) + 71;
            printf ("Random generated number:\t%d\n", randNum_n);
            break;
        }
        case 9: {
            randNum_f = (rand() / (float)RAND_MAX) * 1.999f;
            printf ("Random generated number:\t%f\n", randNum_f);
            break;
        }
        case 10: {
            randNum_f = (rand() / (float)RAND_MAX) * (sqrt(82) - 0.001 - sqrt(17)) + sqrt(17);
            printf ("Random generated number:\t%f\n", randNum_f);
            break;
        }
        default:
            break;
        }
        printf ("Do you want to continue? [1 - Yes]\n");
        scanf ("%d", &isOpen);
    }
    return 0;
}