#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
    srand (time(NULL));
    rand ();
    int X[12];
    for (int k = 0; k < 12; k++) {
        X[k] = rand() % 256 - 128;
        printf ("%d\t", X[k]);
    }

    int B[3] = {X[0], X[4], X[8]};

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            if (X[i*4 + j] > B[i]) {
                B[i] = X[i*4 + j];
            }
        }
    }
    printf ("\n\n");

    int min = B[0];
    for (int x = 0; x < 3; x++) {
        if (min > B[x]) {
            min = B[x];
        }
        printf ("%d\t", B[x]);
    }

    printf ("\n\nmin:\t%d", min);
    return 0;
}