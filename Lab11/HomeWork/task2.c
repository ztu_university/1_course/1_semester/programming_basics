#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
    printf ("Set m n k\nk in range 3-10\n");
    int m, n, k;
    scanf ("%d %d %d", &m, &n, &k);
    srand(time(NULL));
    if (k <= 10 && k >= 3 && m > 0 && n > 0) {
        for (int i = 1; i <= m; i++) {
            printf ("%d\t", rand() % (500 - 125) + 125);
            if (i % k == 0) {
                printf ("\n");
            }
        }
        printf ("\n");
        for (int i = 1; i <= n; i++) {
            printf ("%.1lf\t", (rand() / (double)RAND_MAX) * (15 - 10) + 10);
            if (i % k == 0) {
                printf ("\n");
            }
        }
    }
    return 0;
}