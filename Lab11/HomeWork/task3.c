#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void setArr(int arr[], int size, int a, int b);
int hasConditionMatch(int arr[], int size, int value);
int sumOfElementsMatchingCondition(int arr[], int size);
void showData(int arr[], int size, int leftBound, 
              int rightBound, int zeroes, int sumAfterMin);

int main(void) {
    printf ("Set array size:\t");
    int N;
    scanf ("%d",&N);
    int arr[N];
    printf ("Set range [a, b], a < b:\t");
    int a, b;
    scanf ("%d %d",&a, &b);
    if (N > 0 && a < b) {
        setArr(arr, N, a, b);
        int numOfElements = hasConditionMatch(arr, N, 0);
        int sumOfElements = sumOfElementsMatchingCondition(arr, N);
        showData(arr, N, a, b, numOfElements, sumOfElements);
    }
    return 0;
}

void setArr(int arr[], int N, int a, int b) {
    srand (time(NULL));
    rand ();
    while (N > 0) {
        --N;
        arr[N] = rand() % (b - a) + a;
    }
}

int hasConditionMatch(int arr[], int N, int value) {
    int counter = 0;
    while (N > 0) {
        --N;
        if (arr[N] == value) {
            counter++;
        }
    }
    return counter;
}

int sumOfElementsMatchingCondition(int arr[], int N) {
    int sum = 0;
    int idx = 0;
    int i = 0;
    int min = arr[i];
    while (i < N) {
        if (min > arr[i]) {
            min = arr[i];
            idx = i;
        }
        i++;
    }
    idx++;
    while (idx < N) {
        sum += arr[idx];
        idx++;
    }
    return sum;
}

void showData(int arr[], int N, int a, 
              int b, int zeroes, int sumAfterMin) {
    printf ("\n\nArray size:\t\t%d\n", N);
    printf ("Left bound (a):\t%d\nRight bound (b):\t%d\nArray:\n", a, b);
    int i = 0;
    while (i < N) {
        printf ("%d\t", arr[i]);
        i++;
    }
    printf ("\nNumber of zeroes in array:\t%d\n"
            "Sum of arr elements after min:\t%d\n", 
            zeroes, sumAfterMin);
}