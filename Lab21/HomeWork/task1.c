#include <stdio.h>
#include <string.h>
const int buffSize = 255;

int main() {
    char str[buffSize];
    gets(str);
    int strLen = strlen(str);
    int left_bracket_counter = 0;
    int right_bracket_counter = 0;

    for (int i = 0; i < strLen; i++) {
        if (str[i] == '('){
            left_bracket_counter++;
        }
        if (str[i] == ')'){
            right_bracket_counter++;
        }    
    }

    printf("'('\t-\t%d\n')'\t-\t%d\n",
            left_bracket_counter, right_bracket_counter);
    
    return 0;
}