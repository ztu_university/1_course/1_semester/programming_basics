#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

const int buffSize = 256;
const int max_sentences = 64;

// Check if letter is vowel
int isVowel(char c) {
    c = tolower(c);
    return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');
}

int main() {
    char str[max_sentences][buffSize];
    int counter = 0;

    for (int i = 0; i < max_sentences; i++) {
        // fixed size
        fgets(str[i], buffSize, stdin);
        counter = i;

        int len = strlen(str[i]);
        if (len && (str[i][len - 1] == '\n')) {
            str[i][len - 1] = 0;
        }

        if (isspace(str[i][0])) {
            break;
        }
    }

    int maxSentence_idx = 0;
    int len1 = strlen(str[0]);
    for (int i = 1; i < counter; i++) {
        int len2 = strlen(str[i]);
        if (len1 < len2) {
            maxSentence_idx = i;
        }
    }

    printf("The biggest sentence is %d:\t%s\n", maxSentence_idx + 1, str[maxSentence_idx]);
    char* words;
    int wordsCounter[counter];
    char sorted_words[max_sentences][buffSize];
    int word_idx = 0;
    for (int i = 0; i < counter; i++) {
        int counter_i = 0;
        words = strtok(str[i], " ");
        while (words != NULL) {
            counter_i++;
            int len = strlen(words);
            int res = isVowel (words[len - 2]);
            if (len >= 2 && res) {
                words[0] = 0;
            } else {
                strcpy(sorted_words[word_idx], words);
                word_idx++;
            }            
            words = strtok(NULL, " ");
        }
        wordsCounter[i] = counter_i;
        printf("%d sentence has - {%d} words\n", i + 1, wordsCounter[i]);
    }

    printf ("\nWords with no vowels in the word's (len - 1) pos\n\n");

    for (int i = 0; i < word_idx; i++) {
        printf ("%s ", sorted_words[i]);
    }
    return 0;
}