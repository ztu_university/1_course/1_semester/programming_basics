#include <stdio.h>
#include <string.h>
#include <windows.h>

const int msgBuffSize = 64;
const int morseMsgBuffSize = 384;

void playMorseMessage(char msg[], int size) {
    for (int i = 0; i < size; i++) {
        if (msg[i] == '-') {
            Beep(500, 500);
        } else if (msg[i] == '.') {
            Beep(500, 150);
        } else if (msg[i] == ' ') {
            Sleep(650);
        }
    }
}

void printCodeWord(char word[], char morseMessage[]) {
    char lettersMorse[26][6] = 
    {" .-", " -...", " -.-.", " -..", " .", " ..-.", 
    " --.", " ....", " ..", " .---", " -.-", " .-..", 
    " --", " -.", " ---", " .--.", " --.-", " .-.", 
    " ...", " -", " ..-", " ...-", " .--", " -..-", 
    " -.--", " --.."};
    char digitsMorse[10][7] =
    {" -----", " .----", " ..---", " ...--", 
    " ....-", " .....", " -....", " --...", 
    " ---..", " ----."};

    strlwr(word);
    int len = strlen(word);
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < 26; j++) {
            if (j == 25 && word[i] != '\n' && word[i] != ' ') {
                printf ("%c  ", word[i]);
                word[i] = '\0';
                break;
            }
            else if (word[i] == 97 + j) {
                printf("%s  ", lettersMorse[j]);
                strcat(morseMessage, lettersMorse[j]);
                break;
            }
            else if ((word[i] == 48 + j) && (j < 10)) {
                printf("%s  ", digitsMorse[j]);
                strcat(morseMessage, digitsMorse[j]);
                break;
            } 
        }
    }
    printf("\n");
}


int main () {
    char message[msgBuffSize];
    printf("Type a simple message which contains "
           "letters [A-X] or [a-x] and digits [0-9]\n");
    fgets(message, msgBuffSize, stdin);
    
    char* message_word;
    char morseMsg[morseMsgBuffSize];
    message_word = strtok(message, " ");
    while (message_word != NULL) {
        printCodeWord(message_word, morseMsg);
        message_word = strtok(NULL, " ");
    }
    int morseMsgSize = strlen(morseMsg);
    playMorseMessage(morseMsg, morseMsgSize);
    return 0;
}