#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const int buffSize = 256;
const int max_words = 64;

int main() {
    char str[buffSize];
    gets(str);
    
    char* word;
    word = strtok(str, " ");

    char sorted_words[max_words][buffSize];
    int word_idx = 0;
    while (word != NULL) {
        strcpy(sorted_words[word_idx], word);
        word_idx++;
        word = strtok(NULL, " ");
    }

    int word_count = word_idx - 1;
    for (int i = 0; i < word_idx; i++) {
        for (int j = 0; j < word_count; j++) {
            int compare_res = strcmp(sorted_words[j], sorted_words[j + 1]);
            if (compare_res > 0) {
                char temp[buffSize];
                strcpy(temp, sorted_words[j]);
                strcpy(sorted_words[j], sorted_words[j + 1]);
                strcpy(sorted_words[j + 1], temp);
            }
        }
    }
    
    for (int i = 0; i < word_idx; i++) {
        printf ("%s ", sorted_words[i]);
    }
    return 0;
}