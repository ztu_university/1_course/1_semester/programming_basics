#include <stdio.h>
#include <string.h>
#include <windows.h>

void switchGrade(int grade) {
    switch (grade) {
        case 2:
            printf("�����������\n");
            break;
        case 3:
            printf("���������\n");
            break;
        case 4:
            printf("�����\n");
            break;
        case 5:
            printf("������\n");
            break;
        default:
            printf("³��������� ���� ������\n");
    }
}

void arrayGrade(int grade) {
    const char grades[][25] = {"³��������� ���� ������", "�����������", "���������", "�����", "������"};

    if (grade >= 2 && grade <= 5) {
        printf("%s\n", grades[grade - 1]);
    } else {
        printf("%s\n", grades[0]);
    }
}

void reverseArrayGrade(char* input) {
    const char* grades[] = {"�����������", "���������", "�����", "������"};

    int grade = -1;
    for (int i = 0; i < 4; ++i) {
        if (strcmp(input, grades[i]) == 0) {
            grade = i + 2;
            break;
        }
    }

    if (grade != -1) {
        printf("³������� ������� ��������: %d\n", grade);
    } else {
        printf("������� ���\n");
    }
}

int main() {
    SetConsoleOutputCP(1251);
    SetConsoleCP(1251);
    int grade;
    char input[20];

    printf("������ ������ (�� 2 �� 5): ");
    scanf("%d", &grade);
    
    printf("\n�������� a:\n");
    switchGrade(grade);

    printf("\n�������� b:\n");
    arrayGrade(grade);

    printf("\n�������� c:\n");
    printf("������ ������ � ������ ����� ������: ");
    scanf("%s", input);
    reverseArrayGrade(input);

    return 0;
}
