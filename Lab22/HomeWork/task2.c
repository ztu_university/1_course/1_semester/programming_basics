#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int buffSize = 128;
const char key[] = " .,;\t\n";

void getStr(char initString[buffSize]);
int palindrom(char word[buffSize]);
void alphOrder_repeats(char string[]);
void getSubStrRepeats(char subStringList[][buffSize], char* subString, int sub_str_idx,
                     int listSize, char repStr_collector[][buffSize],
                     int* collector_size, int* maxRepeat
                     );

int main() {
    char __str[buffSize];
    getStr(__str);
    alphOrder_repeats(__str);
    return 0;
}

void getStr(char initString[buffSize]) {
    printf("Type something up to %d symbols:\n", buffSize);
    fgets(initString, buffSize, stdin);
}

void alphOrder_repeats(char string[]) {
    int strLen = strlen(string);

    char** ordered_str;
    ordered_str = (char**)malloc(sizeof(char*) * buffSize / 2);
    for (int i = 0; i < buffSize / 2; i++) {
        ordered_str[i] = (char*)malloc(sizeof(char) * buffSize);
        if (ordered_str[i] == NULL) {
            printf("Couldn't allocate memory at address %p\n", &ordered_str[i]);
            system("Exit");
        }
    }
    
    int token_counter = 0;
    char* str_token = strtok(string, key);

    while (str_token) {
        strcpy(ordered_str[token_counter], str_token);
        token_counter++;
        str_token = strtok(NULL, key);
    }


    for (int i = 0; i < token_counter; i++) {
        for (int j = 0; j < token_counter - 1; j++) {
            if (strcmp(ordered_str[j], ordered_str[j + 1]) > 0) {
                char temp_token[buffSize];
                strcpy(temp_token, ordered_str[j]);
                strcpy(ordered_str[j], ordered_str[j + 1]);
                strcpy(ordered_str[j + 1], temp_token);
            }
        }
    }

    memset(string, 0, strLen);
    int length;
    char init_ordered_str[token_counter][buffSize];
    for (int i = 0; i < token_counter; i++) {        
        strcpy(init_ordered_str[i], ordered_str[i]);
        length = strlen(ordered_str[i]);
        ordered_str[i][length] = ' '; // overwrite null termination
        ordered_str[i][length + 1] = '\0'; // add a new null termination
        strcat(string, ordered_str[i]);
    }
    char collector[token_counter][buffSize];
    int collector_size = 0;

    int max_sub_str_idx = 0;
    for (int i = 0; i < token_counter; i++) {    
        getSubStrRepeats(init_ordered_str, init_ordered_str[i], i, token_counter, collector, &collector_size, &max_sub_str_idx);
    }
    printf ("\n\nThe word '%s' repeats more than others\n", init_ordered_str[max_sub_str_idx]);
    
    printf ("\nAlphabetic order:\n");
    puts(string);

    printf ("\nNo repeats:\n");

    for (int i = 0; i < collector_size; i++) {
        printf ("%s ", collector[i]);
    }

    printf ("\n\nPalindromes:\n");
    int pal_counter = 0;
    int res;
    for (int i = 0; i < collector_size; i++) {
        if (strlen(collector[i]) >= 2) {
            res = palindrom(collector[i]);
            if (res) {
                printf ("'%s'\t\t is a palindrome!\n", collector[i]);
            }
            pal_counter += res;
        }
    }
    printf ("Total number of palindromes:\t%d\n", pal_counter);

    for (int i = 0; i < buffSize / 2; i++) {        
        free(ordered_str[i]);
    }
    free(ordered_str);
}

void getSubStrRepeats(char string[][buffSize], char* subString, int sub_str_idx,
                     int str_len, char collector[][buffSize],
                     int* n, int* max_sub_str_idx)
{
    int res = 0;
    int max = 0;
    for (int i = 0; i < str_len; i++) {
        if (strcmp(subString, string[i]) == 0 && i != sub_str_idx) {
            res++;
        }
    }
    if (res > 0) {
        if (max < res) {
            max = res;
            *max_sub_str_idx = sub_str_idx;
        }
        strcpy(collector[*n], subString);
        if (strcmp(collector[*n - 1], collector[*n]) != 0) {
            printf ("\nWord '%s' repeats\t\t%d times", subString, res);
            *n += 1;
        } else {
            memset(collector[*n], 0, str_len);
        }
    } else {
        strcpy(collector[*n], subString);
        *n += 1;
        printf ("\nWord '%s' repeats\t\t%d times", subString, res);
    }
}

int palindrom(char word[buffSize]) {
    int len = strlen(word);
    int len_ = len / 2;
    int res = 1;
    for (int i = 0; i < len_; i++) {
        if (word[i] != word[len - 1 - i]) {
            res = 0;
            break;
        }
    }

    return res;
}