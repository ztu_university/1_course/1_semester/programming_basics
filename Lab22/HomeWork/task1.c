#include <stdio.h>
#include <string.h>

const int buffSize = 256;

int main () {
    printf("Type something:\n");
    char str[buffSize];
    fgets(str, buffSize, stdin);
    char key[] = " .,?=+";

    // char* pch;
    // pch = strpbrk (str, key);
    // while (pch != NULL) {
    //     *pch = ' ';
    //     pch = strpbrk(pch + 1, key);
    // }
    // puts(str);
    
    char* word = strtok(str, key);
    while (word != NULL) {
        puts(word);
        word = strtok(NULL, key);
    }

    return 0;
}