#include <stdio.h>

// DigitN can handle only 2-byte signed integers
int DigitN(signed long K, int N) {
    if (K >= 0 && K <= __INT16_MAX__ && N > 0) {
        int counter = 0;
        int temp = K;
        if (K == 0 && N == 1) {     // 0 also a number
            return 0;
        }
        while (temp > 0) {
            temp /= 10;
            counter++;
        }
        if (counter < N) {
            printf ("\nThis number has less than %d digits!\n", N);
            return -1;
        }
        for (int i = 1; i < N; i++) {
            K /= 10;
        }
        return K % 10;
    } else {
        if (K > __INT16_MAX__) {
            printf ("Your number is larger than %d!\n", __INT16_MAX__);
            return -1;
        }
        printf ("Number %d is negative!\n", K);
        return -1;
    }
}

int main() {
    signed long k;
    int n, res;
    for (int i = 1; i <= 5; i++) {
        printf ("\nSet number (=> 0 and <= %d):\t", __INT16_MAX__);
        scanf ("%li", &k);
        n = i;
        res = DigitN (k, n);
        printf ("n = %d\tres = %d\n", n, res);
    }
    return 0;
}