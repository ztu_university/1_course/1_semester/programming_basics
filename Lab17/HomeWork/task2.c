#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void setArr(float* arr, int n) {
    srand(time(0));
    rand();
    for (int i = 0; i < n; i++) {
        arr[i] = (rand() / (float)RAND_MAX) * (15.0f + 15.0f) - 15.0f; // -15 15
    }
}

void printArr(float arr[], int len) {
    for (int i = 0; i < len; i++) {
        if (i % 6 == 0) {
            printf ("\n");
        }
        printf ("%f ", arr[i]);
    }
    printf ("\n");
}

void foo(float arr[], int n) {
    for (int i = 0; i < n; i++) {
        if (arr[i] >= 0 && arr[i] <= 3) {
            arr[i] = 3 * arr[i];
        } else if (abs(arr[i]) > 7) {
            arr[i] = arr[i] - 4;
        }
    }
}

int main() {
    printf ("Set size of array (> 0):\t");
    int len;
    scanf ("%d", &len);
    if (len > 0) {
        float arr[len];
        setArr(arr, len);
        printf ("\nGenerated array:\n");
        printArr(arr, len);
        foo(arr, len);
        printf ("\nArray after calling foo:\n");
        printArr(arr, len);
    }
    printf ("\n");
    return 0;
}