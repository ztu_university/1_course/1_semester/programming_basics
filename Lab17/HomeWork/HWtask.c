#include <stdio.h>
#include <math.h>

float Leng     (float x_a, float y_a, float x_b,
                float y_b);

float Perim    (float x_a, float y_a, float x_b, 
                float y_b, float x_c, float y_c);

float Area     (float x_a, float y_a, float x_b, 
                float y_b, float x_c, float y_c);

float Dist     (float x_p, float y_p, float x_a, 
                float y_a, float x_b, float y_b);

void  Altitudes(float x_a, float y_a, float x_b, 
                float y_b, float x_c, float y_c, 
                float* h_a, float* h_b, float* h_c);

/* main */
int main() {
    printf("Set\nx_a, y_a\nx_b, y_b\nx_c, y_c\nx_d, y_d:\t");
    float x_a, y_a, x_b, y_b, x_c, y_c, x_d, y_d;
    scanf("%f %f %f %f %f %f %f %f", 
          &x_a, &y_a, &x_b, &y_b, 
          &x_c, &y_c, &x_d, &y_d); // 8
    
    float len_ab, len_ac, len_ad;
    len_ab = Leng(x_a, y_a, x_b, y_b);                  // AB
    len_ac = Leng(x_a, y_a, x_c, y_c);                  // AC
    len_ad = Leng(x_a, y_a, x_d, y_d);                  // AD

    float perim_abc, perim_abd, perim_acd;
    perim_abc = Perim(x_a, y_a, x_b, y_b, x_c, y_c);    // P_abc
    perim_abd = Perim(x_a, y_a, x_b, y_b, x_d, y_d);    // P_abd
    perim_acd = Perim(x_a, y_a, x_c, y_c, x_d, y_d);    // P_acd

    float area_abc, area_abd, area_acd;
    area_abc = Area(x_a, y_a, x_b, y_b, x_c, y_c);      // S_abc
    area_abd = Area(x_a, y_a, x_b, y_b, x_d, y_d);      // S_abd
    area_acd = Area(x_a, y_a, x_c, y_c, x_d, y_d);      // S_acd

    printf("\nx_a: %.3f, y_a: %.3f, x_b: %.3f, y_b: %.3f, x_c: %.3f, y_c: %.3f, x_d: %.3f, y_d: %.3f\n", 
            x_a, y_a, x_b, y_b, x_c, y_c, x_d, y_d);
    printf("len_ab: %.3f, len_ac: %.3f, len_ad: %.3f\n", len_ab, len_ac, len_ad);
    printf("perim_abc: %.3f, perim_abd: %.3f, perim_acd: %.3f\n", perim_abc, perim_abd, perim_acd);
    printf("area_abc: %.3f, area_abd: %.3f, area_acd: %.3f\n", area_abc, area_abd, area_acd);

    float height_1 = 0.0f;
    float height_2 = 0.0f;
    float height_3 = 0.0f;

    // heights for ABC
    Altitudes(x_a, y_a, x_b, y_b, x_c, y_c, &height_1, &height_2, &height_3);
    printf("\nheight_A: %.3f, height_B: %.3f, height_C: %.3f\n\n", height_1, height_2, height_3);

    // heights for ABD
    Altitudes(x_a, y_a, x_b, y_b, x_d, y_d, &height_1, &height_2, &height_3);
    printf("height_A: %.3f, height_B: %.3f, height_D: %.3f\n\n", height_1, height_2, height_3);

    // heights for ACD
    Altitudes(x_a, y_a, x_c, y_c, x_d, y_d, &height_1, &height_2, &height_3);
    printf("height_A: %.3f, height_C: %.3f, height_D: %.3f\n\n", height_1, height_2, height_3);

    return 0;
}
/* !main */

float Leng     (float x_a, float y_a, float x_b,
                float y_b) {
    float len = sqrt(pow(x_a - x_b, 2) + pow(y_a - y_b, 2));
    return len;
}

float Perim    (float x_a, float y_a, float x_b, 
                float y_b, float x_c, float y_c) {

    float sideAB = Leng(x_a, y_a, x_b, y_b);
    float sideBC = Leng(x_b, y_b, x_c, y_c);
    float sideCA = Leng(x_c, y_c, x_a, y_a);

    float perim = sideAB + sideBC + sideCA;
    return perim;
}

float Area     (float x_a, float y_a, float x_b, 
                float y_b, float x_c, float y_c) {

    float sideAB = Leng(x_a, y_a, x_b, y_b);
    float sideBC = Leng(x_b, y_b, x_c, y_c);
    float sideCA = Leng(x_c, y_c, x_a, y_a);

    // Знаходження периметра трикутника
    float perim = Perim(x_a, y_a, x_b, y_b, x_c, y_c);

    // Знаходження напівпериметра
    float p = perim / 2.0f;
    // Обчислення площі трикутника за формулою Герона
    float triangleArea = sqrt(p * (p - sideAB) * (p - sideBC) * (p - sideCA));

    return triangleArea;
}

float Dist     (float x_p, float y_p, float x_a, 
                float y_a, float x_b, float y_b) {
    
    float area_pab = Area(x_p, y_p, x_a, y_a, x_b, y_b);

    // Знаходження довжини відрізка AB
    float lenAB = Leng(x_a, y_a, x_b, y_b);
    // Обчислення відстані D(P, AB) за формулою
    float dist_p_AB = 2.0f * area_pab / lenAB;

    return dist_p_AB;
}

void  Altitudes(float x_a, float y_a, float x_b, 
                float y_b, float x_c, float y_c, 
                float* h_a, float* h_b, float* h_c) {
    
    *h_a = Dist(x_a, y_a, x_c, y_c, x_b, y_b);          // from A to CB
    *h_b = Dist(x_b, y_b, x_c, y_c, x_a, y_a);          // from B to CA
    *h_c = Dist(x_c, y_c, x_a, y_a, x_b, y_b);          // from C to AB
}