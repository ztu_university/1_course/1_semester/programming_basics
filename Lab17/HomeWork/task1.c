#include <stdio.h>

void printStar(int len) {
    for (int i = 0; i < len; i++)
        printf ("%c", '*');
}

int main() {
    printf ("Set length of row (> 0):\t");
    int len;
    scanf ("%d", &len);
    if (len > 0) {
        printStar (len);
    }
    printf ("\n");
    return 0;
}