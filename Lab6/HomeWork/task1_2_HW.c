#include "stdio.h"
#define _USE_MATH_DEFINES
#include "math.h"
#include "windows.h"
const double e = M_E;

int main()
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);

  printf ("Choose which task you want to see {1, 2, 3}:\t");
  int option;
  scanf ("%d", &option);
  switch (option)
  {
  case 1:
  {
    printf ("\ntask 1\nSet number from 0-9\n");
    int number;
    scanf ("%d", &number);
    switch (number)
    {
    case 0:
      printf ("%d - ����", number);
      break;
    case 1:
      printf ("%d - ����", number);
      break;
    case 2:
      printf ("%d - ���", number);
      break;
    case 3:
      printf ("%d - ���", number);
      break;
    case 4:
      printf ("%d - ������", number);
      break;
    case 5:
      printf ("%d - �'���", number);
      break;
    case 6:
      printf ("%d - �����", number);
      break;
    case 7:
      printf ("%d - ��", number);
      break;
    case 8:
      printf ("%d - ���", number);
      break;
    case 9:
      printf ("%d - ���'���", number);
      break;
    default:
      printf ("\nWrong option");
      break;
    }
    break;
  }
  case 2:
  {
    printf ("\ntask 2\nChoose a function which "
            "you want to use\t\t"
            "1. sin(x)  2. x^2  3. e^x\n");
    int number2;
    scanf ("%d", &number2);

    printf ("\nSet x and y\t");
    double x, y;
    scanf ("%lf %lf", &x, &y);

    double func;
    if (number2 == 1)
      func = sin(x);
    else if (number2 == 2)
      func = x * x;
    else if (number2 == 3)
      func = pow(e, x);
    else
      break;
    
    if (func > 0.0)
      printf ("\na [1] = %lf", ((x * x + func * func) / y));
    else if (func < 0.0)
      printf ("\na [2] = %lf", ((log(fabs(pow(func, 3.0)))) + cos(func))); 
    else
      printf ("\na [3] = %lf", pow(sin(y) * sin(y), 1.0/3.0));
    break;
  }
  case 3:
  {
    printf ("\nHome work\nEnter your age (20-69)");
    char buf[2];

  scanf ("%s", buf);

  if (buf[0] == 50)   // 2
    printf ("\n�������� ");
  else if (buf[0] == 51)
    printf ("\n�������� ");
  else if (buf[0] == 52)
    printf ("\n����� ");
  else if (buf[0] == 53)
    printf ("\n�'������� ");
  else if (buf[0] == 54)
    printf ("\n��������� ");
  else
  {
    printf ("\nwrong age");
    break;
  }

  if (buf[1] == 48)   // 0
    printf ("����");
  else if (buf[1] == 49)
    printf ("���� ��");
  else if (buf[1] == 50)
    printf ("��� ����");
  else if (buf[1] == 51)
    printf ("��� ����");
  else if (buf[1] == 52)
    printf ("������ ����");
  else if (buf[1] == 53)
    printf ("�'��� ����");
  else if (buf[1] == 54)
    printf ("����� ����");
  else if (buf[1] == 55)
    printf ("�� ����");
  else if (buf[1] == 56)
    printf ("��� ����");
  else if (buf[1] == 57)
    printf ("���'��� ����");
  else
  {
    printf ("\nwrong age");
    break;
  }
    break;
  }
  default:
    printf ("\nWrong option");
    break;
  }
  
  return 0;
}