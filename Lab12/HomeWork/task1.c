#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void setArr(int arr[], int size);
void printArray (int arr[], int size);
int findTheClosestSum(int value, int arr[], int size);

int main(void) {
    printf ("Set R and N:\t");
    int r, n;
    scanf ("%d %d", &r, &n);

    int arr[n];
    setArr(arr, n);
    printArray(arr, n);
    int res = findTheClosestSum(r, arr, n);
    printf ("res: %d", res);
    return 0;
}

void setArr(int arr[], int n) {
    srand (time(NULL));
    rand();
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 100;
    }
}

void printArray (int arr[], int N) {
    int i = 0;
    printf ("\nArray size = %d\n", N);
    while (i < N) {
        if (i == (N - 1)) {
            printf("%d\n", arr[i]);
        } else {
            printf("%d, ", arr[i]);
        }
        i += 1;
    }
}

int findTheClosestSum(int r, int arr[], int n) {
    int currSum = arr[0] + arr[1];
    int currDif = abs(r - currSum);
    int tempDif = currDif;
    int theClosestSum;
    
    int i = 0, j = 0;
    int i_, j_;
    while (i < n) {
        j = i + 1;
        while (j < n) {
            if (arr[i] == arr[j]) {
                j++;
                continue;
            } else {
                currSum = arr[i] + arr[j];
                currDif = abs(r - currSum);
                
                if (currSum == r) {
                    printf("1 elem = %d\t2 elem = %d\n", arr[i], arr[j]);
                    return currSum;
                } else if (currSum != r && tempDif > currDif) {
                    tempDif = currDif;
                    theClosestSum = currSum;
                    i_ = i;
                    j_ = j;
                }
            }
            j++;
        }
        i++;
    }

    printf("1 elem = %d\t2 elem = %d\n", arr[i_], arr[j_]);
    return theClosestSum;
}