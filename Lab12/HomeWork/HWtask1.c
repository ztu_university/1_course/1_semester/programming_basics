#include <stdio.h>

void setArr(int arr[], int size);
void printArray (int arr[], int size);
void sortArr(int arr[], int arrSize);

int main () {
    int arr[11];
    printf ("\nArray size = 11\n");
    setArr      (arr, 11);
    //printArray  (arr, 11);
    sortArr     (arr, 11);
    printf ("\nSorted\n");
    printArray  (arr, 11);
    return 0;
}

void setArr (int arr[], int n) {
    int i = 0;
    while (i < n) {
        scanf ("%d", &arr[i]);
        i++;
    }
}

void printArray (int arr[], int N) {
    int i = 0;
    while (i < N) {
        if (i == (N - 1)) {
            printf("%d\n", arr[i]);
        } else {
            printf("%d, ", arr[i]);
        }
        i += 1;
    }
}

// odd-even sort
void sortArr (int arr[], int d) {
    int sorted = 0;
    while (!sorted) {
        sorted = 1;
        for (int i = 1; i < d-1; i+=2) {
            if (arr[i] < 0 && arr[i+1] >= 0) {
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
                sorted = 0;
            }
        }
        for (int i = 0; i < d-1; i+=2) {
            if (arr[i] < 0 && arr[i+1] >= 0) {
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
                sorted = 0;
            }
        }
    }
}