#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void setArr(int arr[], int size);
void printArray (int arr[], int size);
void sortArr(int arr[], int arrSize);


int main () {
    printf ("Set array:\t");
    int n = 14;
    int arr[n];
    printf ("\nArray size = 14\n");
    setArr      (arr, n);
    printArray  (arr, n);
    sortArr     (arr, n);
    printf ("\nSorted\n");
    printArray  (arr, n);
    return 0;

}

void setArr (int arr[], int n) {
    srand(time(NULL));
    rand();
    for (int i = 0; i < n; i++) {
       arr[i] = rand() % (5 + 5) - 5; // -5 ; 5
    }
}

void printArray (int arr[], int N) {
    int i = 0;
    while (i < N) {
        if (i == (N - 1)) {
            printf("%d\n", arr[i]);
        } else {
            printf("%d, ", arr[i]);
        }
        i += 1;
    }
}

void sortArr (int arr[], int d) {
    int sorted = 0;
    while (!sorted) {
        sorted = 1;
        for (int i = 1; i < d-1; i+=2) {
            if (arr[i] <= arr[0] && arr[i+1] >= arr[0]) {
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
                sorted = 0;
            }
        }
        for (int i = 2; i < d-1; i+=2) {
            if (arr[i] <= arr[0] && arr[i+1] >= arr[0]) {
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
                sorted = 0;
            }
        }
    }
    int count = 0;
    for (int i = 1; i < d; i++) {
        if (arr[0] <= arr[i]) {
            count++;
        }
    }
    int temp;
    for (int i = 0; i < count; i++) {
        temp = arr[i];
        arr[i] = arr[i+1];
        arr[i+1] = temp;
        
    }
}