// Suggested that all numbers are positive except of 0
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void setArr(int arr[], int size);
void printArray (int arr[], int size);
void readWriteArray (int arr[], int size);
void generateNewArr (int arr[], int size, int arr2[], int size2);

int main () {
    printf ("Set N:\t");
    int n;
    scanf ("%d", &n);
    int arr[n];
    setArr(arr, n);
    printArray(arr, n);
    readWriteArray(arr, n);
    return 0;
}

void setArr(int arr[], int n) {
    srand (time(NULL));
    rand();
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 9 + 1;
    }
}

void printArray (int arr[], int N) {
    int i = 0;
    printf ("Generated array size = %d\n", N);
    while (i < N) {
        if (i == (N - 1)) {
            printf("%d\n", arr[i]);
        } else {
            printf("%d, ", arr[i]);
        }
        i += 1;
    }
}

void readWriteArray (int arr[], int n) {
    int arrSize = 0;
    int counter = 1;
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (arr[i] == arr[j]) {
                counter++;
                if ((counter >= 3)) {
                    arr[i] = -abs(arr[i]);
                    for (int l = 0; l < n; l++) {
                        if (arr[l] == -arr[i]) {
                            arr[l] = arr[i];
                        }
                    }
                } 
            }
        }
    counter = 1;
    }
    for (int i = 0; i < n; i++) {
        if (arr[i] <= 0) {
            arrSize++;
        }
    }
    int newArr[arrSize];
    generateNewArr (arr, n, newArr, arrSize);
}

void generateNewArr (int arr[], int n, int newArr[], int arrSize) {
    for (int i = 0, j = 0; i < n; i++) {
        if (arr[i] <= 0) {
            newArr[j] = -arr[i];
            j++;
        }
    }
    printArray (newArr, arrSize);
}
