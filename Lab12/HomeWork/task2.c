#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void setArr(int arr[], int size);
// Descending sort order
void sortArrL(int arr[], int arrSize);
// Ascending sort order
void sortArrR(int arr[], int arrSize);
void printSortedArray (int arrD[], int sizeD);
void copyToArr(int arrD[], int arrA[], int arrB[], int arrC[],
                    int sizeD, int sizeA, int sizeB, int sizeC);

int main () {
    printf ("Set N_A, N_B and N_C:\t");
    int a, b, c;
    scanf ("%d %d %d", &a, &b, &c);
    int arrA[a];
    int arrB[b];
    int arrC[c];
    int d = 0;
    // corrections
    if (abs(a) == a)
        d += a;
    else
        a = 0;
    if (abs(b) == b)
        d += b;
    else
        b = 0;
    if (abs(c) == c)
        d += c;
    else
        c = 0;
    // !corrections

    int arrD[d];
    srand (time(NULL));
    rand();
    setArr(arrA, a); sortArrL(arrA, a); printSortedArray (arrA, a);
    setArr(arrB, b); sortArrL(arrB, b); printSortedArray (arrB, b);
    setArr(arrC, c); sortArrL(arrC, c); printSortedArray (arrC, c);
    copyToArr(arrD, arrA, arrB, arrC, d, a, b, c);
    sortArrR(arrD, d); printSortedArray (arrD, d);
    return 0;
}

void setArr(int arr[], int n) {

    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 100;
    }
}

void copyToArr(int arrD[], int arrA[],
               int arrB[], int arrC[],
               int d, int a, int b, int c) {
    for (int i = 0; i < a; i++) {
        arrD[i] = arrA[i];
    }
    int ab = a + b;
    for (int i = a, j = 0; i < ab; i++, j++) {
        arrD[i] = arrB[j];
    }
    for (int i = ab, j = 0; i < d; i++, j++) {
        arrD[i] = arrC[j];
    }
    for (int i = 0; i < d; i++) {
        printf ("arrD[%d] = %d\n", i, arrD[i]);
    }
}
void sortArrL(int arr[], int N) {
    int sorted = 0;
    while (!sorted) {
        sorted = 1;
        for (int i = 1; i < N-1; i+=2) {
            if (arr[i] < arr[i+1]) {
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
                sorted = 0;
            }
        }
        for (int i = 0; i < N-1; i+=2) {
            if (arr[i] < arr[i+1]) {
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
                sorted = 0;
            }
        }
    }
}

void sortArrR(int arrD[], int d) {
    int sorted = 0;
    while (!sorted) {
        sorted = 1;
        for (int i = 1; i < d-1; i+=2) {
            if (arrD[i] > arrD[i+1]) {
                int temp = arrD[i];
                arrD[i] = arrD[i+1];
                arrD[i+1] = temp;
                sorted = 0;
            }
        }
        for (int i = 0; i < d-1; i+=2) {
            if (arrD[i] > arrD[i+1]) {
                int temp = arrD[i];
                arrD[i] = arrD[i+1];
                arrD[i+1] = temp;
                sorted = 0;
            }
        }
    }
}

void printSortedArray (int arr[], int N) {
    int i = 0;
    while (i < N) {
        if (i == (N - 1)) {
            printf("%d\n", arr[i]);
        } else {
            printf("%d, ", arr[i]);
        }
        i += 1;
    }
}